<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Mailgun, Postmark, AWS and more. This file provides the de facto
    | location for this type of information, allowing packages to have
    | a conventional file to locate the various service credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
        'endpoint' => env('MAILGUN_ENDPOINT', 'api.mailgun.net'),
    ],

    'postmark' => [
        'token' => env('POSTMARK_TOKEN'),
    ],

    'ses' => [
        'key' => env('AWS_ACCESS_KEY_ID'),
        'secret' => env('AWS_SECRET_ACCESS_KEY'),
        'region' => env('AWS_DEFAULT_REGION', 'us-east-1'),
    ],

    'github' => [
        'client_id' => '43f0c722a36eb52a4782',
        'client_secret' => '36f2e16a278cce9a5e41b78e563a3168215b1919',
        'redirect' => 'http://127.0.0.1:8000/github/auth/callback',
    ],

    'linkedin' => [
        'client_id' => '86jk2wo95o73ve',
        'client_secret' => 'j5OGjISgyGbfHNq2',
        'redirect' => 'http://127.0.0.1:8000/auth/callback/linkedin',
    ],



];
