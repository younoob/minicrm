<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use DB;
use Carbon\Carbon;
use App\Models\Summary;

class ItemSellSummarySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $item = [
            '1' => 'Smartphone',
            '2' => 'Computer',
            '3' => 'Smart TV',
            '4' => 'Tablet',
            '5' => 'Airbuds',
            '6' => 'Drone',
            '7' => 'Camera',
            '8' => 'Console',
        ];

        $price = [
            '1' => '1000000',
            '2' => '3000000',
            '3' => '5000000',
            '4' => '7000000',
            '5' => '3500000',
            '6' => '28000000',
            '7' => '21000000',
            '8' => '9800000',
        ];

        foreach($item as $data => $value){
            DB::table('items')->insert([
                'name' => $value,
                'price' => $price[$data],
            ]);
        }

        for($i = 1; $i <= 100; $i++){
            $discount = rand(1, 50);
            $employee_id = rand(1, 50);
            $item_id = rand(1, 8);
            $created_at = Carbon::now()->subHours(rand(1, 55));
            $normal_price = $price[$item_id];
            $piece = $normal_price * ($discount/100);
            $get_off = $normal_price - $piece;

            DB::table('sells')->insert([
                'price' => $normal_price,
                'discount'=> $discount,
                'item_id'=> $item_id,
                'employee_id'=> $employee_id,
                'created_at'=> $created_at,
            ]);


            $summary = Summary::where('employee_id',$employee_id)
                        ->whereDate('created_at' , $created_at)->first();
            if($summary){
                $summary->price_total += $normal_price;
                $summary->discount_total += $piece;
                $summary->total += $get_off;
                $summary->update();
            }
            else{
                DB::table('summaries')->insert([
                    'price_total' => $normal_price,
                    'discount_total'=> $piece,
                    'total' => $get_off,
                    'employee_id'=> $employee_id,
                    'created_at'=> $created_at,
                ]);
            }

        }
    }
}
