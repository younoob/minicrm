<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Imports\CompanyImport;
use App\Imports\EmployessImport;

class ExcelSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        (new CompanyImport)->import('company_mock_data.xlsx', null, \Maatwebsite\Excel\Excel::XLSX);
        (new EmployessImport)->import('employess_mock_data.xlsx', null, \Maatwebsite\Excel\Excel::XLSX);
    }
}
