<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Fragment;

class FragmentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            'Logout' => 'Keluar',
            'Search' => 'Cari',
            'Company' => 'Perusahaan',
            'Employess' => 'Karyawan',
            'User Registration' => 'Pendaftaran Pengguna',
            'More Info' => 'Informasi Lanjutan',
            'Lang' => 'Bhs',
            'Home' => 'Beranda',
            'Details' => 'Rincian',
            'Dashboard' => 'Papan Informasi',
            'Copyright' => 'Hak Cipta',
            'Modified by' => 'Diubah oleh',
            'All rights reserved' => 'Hak cipta dilindungi undang-undang',
            'Version' => 'Versi',
            'Data Company' => 'Data Perusahaan',
            'Create Data Company' => 'Buat Data Perusahaan',
            'Update Data Company' => 'Perbarui Data Perusahaan',
            'Company Details' => 'Rincian Perusahaan',
            'No' => 'No',
            'First Name' => 'Nama Depan',
            'Last Name' => 'Nama Belakang',
            'Company' => 'Perusahaan',
            'Name' => 'Nama',
            'Email' => 'Email',
            'Logo' => 'Logo',
            'Website' => 'Situs',
            'Phone Number' => 'Nomor Telepon',
            'Password' => 'Kata Sandi',
            'Joined' => 'Bergabung',
            'Created By' => 'Dibuat Oleh',
            'Updated By' => 'Diubah Oleh',
            'Action' => 'Aksi',
            'Create' => 'Buat',
            'Submit' => 'Kirimkan',
            'Update' => 'Perbarui',
            'Back' => 'Kembali',
            'Data Employess' => 'Data Karyawan',
            'Create Data Employess' => 'Buat Data Karyawan',
            'Update Data Employess' => 'Perbarui Data Karyawan',
            'Employess Details' => 'Rincian Karyawan',
            'Data Users' => 'Data Pengguna',
            'Role' => 'Wewenang',
        ];

        $data_jp = [
            'Logout' => 'ログアウト',
            'Search' => '探す',
            'Company' => '会社',
            'Employess' => '従業員',
            'User Registration' => 'ユーザー登録',
            'More Info' => '詳しく',
            'Lang' => '語',
            'Home' => 'ベランダ',
            'Details' => '詳細',
            'Dashboard' => 'ダッシュボード',
            'Copyright' => '著作権',
            'Modified by' => '変更された',
            'All rights reserved' => '大丈夫予約',
            'Version' => 'バージョン',
            'Data Company' => '会社のデータ',
            'Create Data Company' => '会社のデータを作る',
            'Update Data Company' => '会社のでーたを変わる',
            'Company Details' => '会社の詳細',
            'No' => '番号',
            'First Name' => '名前',
            'Last Name' => '苗字',
            'Company' => '会社',
            'Name' => '名前',
            'Email' => 'メール',
            'Logo' => '写真',
            'Website' => 'ウェブサイト',
            'Phone Number' => '電話番語',
            'Password' => 'パスワード',
            'Joined' => '参加した',
            'Created By' => 'によって作成された',
            'Updated By' => 'によって更新された',
            'Action' => 'アクション',
            'Create' => '作る',
            'Submit' => '送る',
            'Update' => '変わる',
            'Back' => 'Kembali',
            'Data Employess' => 'バック',
            'Create Data Employess' => '従業員のデータを作る',
            'Update Data Employess' => '従業員のでーたを変わる',
            'Employess Details' => '従業員の詳細',
            'Data Users' => 'ユーザーのでーた',
            'Role' => '役割',
        ];

        foreach ($data as $key => $val) {
            $notfound = array_key_exists($key, $data_jp) == true? $data_jp[$key]:'';
            // $simpan [$key ]= $val; 
            // $simpan_jp [$key ]= $notfound; 
            DB::table('fragments')->insert([
                'key' => $key,
                'id' => $val,
                'jp' => $notfound,            
            ]);
        }
    }
}
