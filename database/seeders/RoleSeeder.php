<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       if (DB::table('roles')->count() == 0) {
           DB::table('roles')->insert([
            [
                'name'  =>  'Admin',
            ],
            [
                'name'  => 'Employee'
            ]
           ]);
       } else {
        echo "\e[31Table is not empty, therefore NOT";
       }
    }
}
