<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use App\Models\User;
use App\Helpers\Enums\UserRole;
use App\Http\Requests\UserRequest;
use Illuminate\Support\Facades\Log;

class AuthenticationController extends Controller
{

    public function view() {
        return view('login');
    }

    public function loginproccess(UserRequest $req){
        $validated = $this->validate($req, [
            'email'     => 'required|email',
            'password'  => 'required|string'
        ]);

        try {
            $credentials = [
                'email'     => $validated['email'],
                'password'  => $validated['password']
            ];

            if (Auth::attempt($credentials)) {
                if (Auth::user()->role == UserRole::ADMIN_HRD) {
                    $req->session()->regenerate();
                    notify()->success('Logged in', 'You are Logged as Admin!');
                    return Redirect::to('/admin');
                }else {

                    return redirect()->back();
                }
            } else {
                notify()->error('Failed', 'Account does not exist!');
                return redirect()->back();
            }
        } catch (\Exception $e) {
            Log::error([
                "Message " . $e->getMessage(),
                "On file " . $e->getFile(),
                "On line " . $e->getLine(),
            ]);

            return $e->getMessage();
        }

    }

    public function logout(Request $req){
        try{

            Auth::logout();
            $req->session()->invalidate();
            $req->session()->regenerateToken();
            notify()->success('Session ended','Logged out');
            return Redirect::to('/');
        } catch (\Exception $e) {
            Log::error([
                "Message " . $e->getMessage(),
                "On file " . $e->getFile(),
                "On line " . $e->getLine(),
            ]);

            return $e->getMessage();
        }
    }

    public function register(){
        return view('register');
    }

    public function register_on(Request $request){

        $validated = $this->validate($request, [
            'name'      => 'required|string',
            'email'     => 'required|email:rfc,dns',
            'password'  => 'required|min:6'
        ]);

        try {
            $register = User::create([
                'name' => $validated['name'],
                'email' => $validated['email'],
                'password' => bcrypt($validated['password']),
                'role' => UserRole::ADMIN_HRD
            ]);

            if($register){
                notify()->success('Hi'.' '.$request->name.' '.'Welcome to CRM System','Your registration complete');
                return Redirect::to('/');
            } else{
                notify()->error('Failed', 'Cannot create a account!');
                return redirect()->back();
            }
        } catch(\Exception $e) {
            Log::error([
                "Message " . $e->getMessage(),
                "On file " . $e->getFile(),
                "On line " . $e->getLine(),
            ]);

            return $e->getMessage();
        }

    }

}
