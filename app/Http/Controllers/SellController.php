<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Sell;
use App\Models\Item;
use Config;
use Session;
use Redirect;
use App\Models\Employess;
use App\Models\Summary;
use DB;
use Validator;
use Carbon\Carbon;

class SellController extends Controller
{
    public function index()
    {
        $sell = Sell::paginate(10);
        return view('back.sell.index', compact('sell'));
    }

    public function indexCreate()
    {
        $item = Item::all();
        $employee = Employess::all();
        return view('back.sell.create', compact('item','employee'));
    }

    public function createSell(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'item_id' => 'required',
            'discount' => 'required',
            'employee_id' => 'required',
        ]);
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator);
        }

        Config::set('app.timezone', 'UTC');

        $item_id = Item::where('id', $request->item_id)->first();
        $data = $item_id->price;

        $discount = $item_id->price * ($request->discount / 100);
        $get_off = $data - $discount;

        $sell = Sell::create([
            'item_id' => $request->item_id,
            'price' => $data,
            'discount' => $request->discount,
            'employee_id' => $request->employee_id,
        ]);

        if($sell->save()){
            $summary = Summary::where('employee_id', $request->employee_id)
            ->whereDate('created_at', Carbon::today())->first();
            
            if($summary) {
                $summary->price_total += $data;
                $summary->discount_total += $discount;
                $summary->employee_id = $request->employee_id;
                $summary->total += $get_off;
                $summary->update(); 
            } else {
                $summary = new Summary();
                $summary->price_total += $data;
                $summary->discount_total += $discount;
                $summary->employee_id = $request->employee_id;
                $summary->total += $get_off;
                $summary->save(); 
            }

            Config::set('app.timezone', Session::get('timezone'));

            return Redirect::to('/admin/sell')->with('success', 'Data Created');
        }else{
            return redirect()->back()->with('error', 'Something else, please try again!');
        }
    }

    public function indexUpdate($id)
    {
        $sell = Sell::find($id);
        $item = Item::all();
        $employee = Employess::all();
        return view('back.sell.update', compact('sell', 'item', 'employee'));
    }

    public function updateSell(Request $request, $id)
    {
        
        Config::set('app.timezone', 'UTC');
        $sell = Sell::find($id);
        $summary = Summary::where('employee_id', $sell->employee_id)->whereDate('created_at', $sell->created_at)->first();
        $item_id = Item::where('id', $request->item_id)->first();
        $price = $item_id->price;
        $discount = $sell->price * ($sell->discount / 100);
        $get_off = $sell->price - $discount;

        if($summary){
            $summary->price_total -= $sell->price;
            $summary->discount_total -= $discount;
            $summary->total -= $get_off;
            $summary->update();
        }

        $summary_update = Summary::where('employee_id', $request->employee_id)->whereDate('created_at', $sell->created_at)->first();

        if($summary_update){
            $summary_update->price_total += $price;
            $summary_update->discount_total += $price * ($request->discount / 100);
            $summary_update->total += $price - ($price * ($request->discount / 100));
            $summary_update->update();
        }else {
            $summary_new = new Summary();
            $summary_new->price_total += $price;
            $summary_new->discount_total += $price - ($request->discount / 100);
            $summary_new->total += $price - ($price * ($request->discount / 100));
            $summary_new->employee_id = $request->employee_id;
            $summary_new->created_at = $summary->created_at;
            $summary_new->save();
        }

        $sumdel = Summary::where('employee_id', $sell->employee_id)->whereDate('created_at', $sell->created_at)->first();

        if($sumdel->discount_total == 0 && $sumdel->price_total == 0 && $sumdel->total == 0){
            $sumdel->delete();
        }

        $sell->update([
            'item_id' => $request->item_id,
            'price' => $price,
            'discount' => $request->discount,
            'employee_id' => $request->employee_id,
        ]);

        if($sell){
            Config::set('app.timezone', Session::get('timezone'));
            return Redirect::to('/admin/sell')->with('success', 'Data Updated');
        }else{
            return redirect()->back()->with('error', 'Something else, please try again!');
        }

        
    }

    public function deleteSell($id)
    {
        $sell = Sell::find($id)->first();

        $price = $sell->price;
        $discount = $price * ($sell->discount / 100);
        $get_off = $price - $discount;

        $summary = Summary::where('employee_id', $sell->employee_id)
        ->whereDate('created_at', $sell->created_at)->first();

        if($summary){
            $summary->price_total -= $price;
            $summary->discount_total -= $discount;
            $summary->total -= $get_off;
            $summary->update();

            $delete_summary = Summary::where('employee_id', $sell->employee_id)
            ->whereDate('created_at', $sell->created_at)->first();

            if($delete_summary->discount_total == 0 && $delete_summary->price_total == 0 && $delete_summary->total == 0){
                $delete_summary->delete();
            }
        }

        if($sell->delete()) {
            return redirect()->back()->with('success', 'Data Deleted');
        } else{
            return redirect()->back()->with('error', "Can't Deleting Data");
        }
    }

    public function searchSell(Request $request)
    {
        $keyword = $request->qsell;

        $sell = Sell::where('price', 'like', "%$keyword%")
                ->orWhere('discount', 'like', "%$keyword%")
                ->orWhereDate('created_at', 'like', "%$keyword%")
                ->orWhereHas('getItemName', function($query) use($keyword){
                    $query->where('name', 'like', "%$keyword%");
                })
                ->orWhereHas('getEmployeeName', function($sub_query) use($keyword){
                    $sub_query->where('fisrt_name', 'like', "%$keyword%");
                })
                ->orWhereHas('getEmployeeName', function($last_query) use($keyword){
                    $last_query->where('last_name', 'like', "%$keyword%");
                })->paginate();

        return view('back.sell.search', ['sell' => $sell], compact('sell', 'keyword'));
    }
}
