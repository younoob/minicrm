<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Item;
use App\Services\ItemService;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Wavey\Sweetalert\Sweetalert;

class ItemController extends Controller
{

    protected $itemService;

    public function __construct()
    {
        $this->itemService = new ItemService;
    }

    public function index()
    {
        $item = Item::orderBy('created_at', 'desc')->paginate(10);
        return view('back.item.index', compact('item'));
    }

    public function indexCreate()
    {
        return view('back.item.create');
    }

    public function createData(Request $request)
    {
        $validated = $this->validate($request, [
            'name'  =>  'required|string',
            'price' =>  'required|numeric'
        ]);

        try {
            $response = $this->itemService->create($validated);
            return $response;
        } catch (\Exception $e){
            Log::error([
                "Message " . $e->getMessage(),
                "On file " . $e->getFile(),
                "On line " . $e->getLine(),
            ]);

            return $e->getMessage();
        }
    }

    public function indexUpdate($id)
    {
        $item = Item::find($id);

        if(!$item){
            Sweetalert::info('Item Not-found!' ,'Info');
            return back();
        }

        return view('back.item.update', compact('item'));
    }

    public function updateData(Request $request, $id)
    {
        $validated = $this->validate($request, [
            'name'  =>  'required|string',
            'price' =>  'required|numeric',
        ]);

        try{
            $response = $this->itemService->update($validated, $id);
            return $response;
        }catch (\Exception $e){
            Log::error([
                "Message " . $e->getMessage(),
                "On file " . $e->getFile(),
                "On line " . $e->getLine(),
            ]);

            return $e->getMessage();
        }
    }

    public function deleteData($id)
    {
        try{
            $data = Item::find($id);
            if (!$data){
                Sweetalert::info('Item Not-found!', 'Info');
                return back();
            }

            $data->delete();
            Sweetalert::success('Item deleted!', 'Success');
            return redirect()->back();
        } catch (\Exception $e){
            Log::error([
                "Message " . $e->getMessage(),
                "On file " . $e->getFile(),
                "On line " . $e->getLine(),
            ]);

            return $e->getMessage();
        }
    }

    public function searchItem(Request $request)
    {
        try {
            $keyword = $request->qitem;

            $item = DB::table('items')
                    ->where('name', 'like', "%$keyword%")
                    ->orWhere('price', 'like', "%$keyword%")->paginate();

            return view('back.item.search', ['item' => $item], compact('item', 'keyword'));
        }catch (\Exception $e){
            Log::error([
                "Message " . $e->getMessage(),
                "On file " . $e->getFile(),
                "On line " . $e->getLine(),
            ]);

            return $e->getMessage();
        }
    }
}
