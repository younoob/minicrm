<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Company;
use App\Models\Employess;

class APIController extends Controller
{
    public function data_employess($id){
        $employess = Employess::where('company_id', $id)->with('getCompanyName', 'getCreatedName', 'getUpdatedName')->get();

        if ($employess->count() > 0) {
            return response()->json([
                'status' => "Success with data",
                'data' => $employess, 
        ]);
        } else {
            return response()->json(['status' => "Employee with company_id = '$id' Not found!"]);
        }
    }
}
