<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Company;
use App\Services\CompanyService;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Log;
use Wavey\Sweetalert\Sweetalert;

class CompanyController extends Controller
{
    protected $companyService;

    public function __construct()
    {
        $this->companyService = new CompanyService;
    }

    public function company()
    {
        $companies = Company::orderBy('created_at', 'desc')->paginate(10);
        return view('back.company.index', compact('companies'));
    }

    public function indexCreate()
    {
        return view('back.company.create');
    }

    public function createCompany(Request $request)
    {
        $validated = $this->validate($request, [
            'name'      =>  'required|string',
            'email'     =>  'required|email:rfc,dns',
            'image'     =>  'required|image|mimes:jpeg,jpg,png,svg|max:1024',
            'website'   =>  'required|url'
        ]);

        $exist = Company::where('email', $validated['email'])->get();

        if(count($exist)>0){
            Sweetalert::error('Email already registered!', 'Failed');
            return redirect()->back();
        }

        try {
            $response = $this->companyService->create($validated, $request->created_by);
            return $response;
        }catch(\Exception $e) {
            Log::error([
                "Message " . $e->getMessage(),
                "On file " . $e->getFile(),
                "On line " . $e->getLine(),
            ]);

            return $e->getMessage();
        }
    }

    public function updateView($id)
    {
        $company = Company::find($id);

        if (!$company) {
            Sweetalert::error('Company Not-found', 'Failed');
            return back();
        }

        return view('back.company.update', compact('company'));
    }

    public function updateCompany(Request $request, $id)
    {
        $validated = $this->validate($request, [
            'name'      =>  'required|string',
            'email'     =>  'required|email',
            'image'     =>  'nullable',
            'website'   =>  'required|url'
        ]);

        try {
            $response = $this->companyService->update($validated, $id, $request->updated_by);
            return $response;
        }catch(\Exception $e) {
            Log::error([
                "Message " . $e->getMessage(),
                "On file " . $e->getFile(),
                "On line " . $e->getLine(),
            ]);

            return $e->getMessage();
        }
    }

    public function deleteCompany($id)
    {
        try{
            $company = Company::findOrFail($id);
            $image_path = $company->image;

            if (File::exists(public_path($image_path))) File::delete(public_path($image_path));

            if($company->delete()){
                Sweetalert::success('Success','Data deleted');
                return redirect()->back();
            }else {
                Sweetalert::error('Data cannot be deleted', 'Failed');
                return redirect()->back();
            }
        }catch(\Exception $e) {
            Log::error([
                "Message " . $e->getMessage(),
                "On file " . $e->getFile(),
                "On line " . $e->getLine(),
            ]);

            return $e->getMessage();
        }
    }

    public function detailsCompany($id)
    {
        $company = Company::find($id);
        return view('back.company.details', compact('company'));
    }

    public function searchCompany(Request $request)
    {
        try{
            $response = $this->companyService->search($request->all());
            return $response;
        }catch(\Exception $e) {
            Log::error([
                "Message " . $e->getMessage(),
                "On file " . $e->getFile(),
                "On line " . $e->getLine(),
            ]);

            return $e->getMessage();
        }
    }

    public function importCompany() {
        try {
            $validation = Validator::make(request()->all(), [
                'file'  => 'required|mimes:xlsx, csv, xls',
            ]);

            if ($validation->fails()) {
                Sweetalert::error(implode($validation->errors()->all()) ,'Failed');
                return redirect()->back();
            }

            $response = $this->companyService->import();
            return $response;
        }catch(\Exception $e) {
            Log::error([
                "Message " . $e->getMessage(),
                "On file " . $e->getFile(),
                "On line " . $e->getLine(),
            ]);

            return $e->getMessage();
        }
    }

}
