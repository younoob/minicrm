<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Employess;
use App\Models\Company;
use App\Services\EmployeeService;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Log;
use Wavey\Sweetalert\Sweetalert;

class EmployeeController extends Controller
{
    protected $employeeService;

    public function __construct()
    {
        $this->employeeService = new EmployeeService;
    }

    public function employessView()
    {
        $employess = Employess::paginate(10);
        return view('back.employess.index', compact('employess'));
    }

    public function indexEmployess()
    {
        $company = Company::all();
        return view('back.employess.create', compact('company'));
    }

    public function createEmployess(Request $request)
    {
        $exist = Employess::where('email', $request->email)->get();

        if (count($exist)>0){
            Sweetalert::error('The email has already!', 'Failed');
            return back();
        }

        $validated = $this->validate($request, [
            'fisrt_name'    =>  'required|string',
            'last_name'     =>  'required|string',
            'company_id'    =>  'required|numeric|min:1',
            'phone'         =>  'required|numeric',
            'email'         =>  'required|email:rfc,dns',
            'password'      =>  'required|min:6'
        ]);

        try {
            $response = $this->employeeService->create($validated, $request->created_by);
            return $response;
        }catch (\Exception $e){
            Log::error([
                "Message " . $e->getMessage(),
                "On file " . $e->getFile(),
                "On line " . $e->getLine(),
            ]);

            return $e->getMessage();
        }
    }

    public function indexUpdateEmployess($id)
    {
        try {
            $employess = Employess::find($id);
            if (!$employess) {
                Sweetalert::info('Employee Not-found', 'Info');
                return back();
            }

            $company = Company::all();
            if (!$company) {
                Sweetalert::info('Company empty!', 'Info');
                return back();
            }

            return view('back.employess.update', compact('employess', 'company'));
        }catch (\Exception $e){
            Log::error([
                "Message " . $e->getMessage(),
                "On file " . $e->getFile(),
                "On line " . $e->getLine(),
            ]);

            return $e->getMessage();
        }
    }

    public function updateEmployess(Request $request, $id)
    {
        $validated = $this->validate($request, [
            'fisrt_name'    =>  'required|string',
            'last_name'     =>  'required|string',
            'company_id'    =>  'required|numeric|min:1',
            'phone'         =>  'required|numeric',
            'email'         =>  'required|email:rfc,dns',
            'password'      =>  'required|min:6'
        ]);

        try {
            $response = $this->employeeService->update($validated, $id, $request->updated_by);
            return $response;
        }catch (\Exception $e){
            Log::error([
                "Message " . $e->getMessage(),
                "On file " . $e->getFile(),
                "On line " . $e->getLine(),
            ]);

            return $e->getMessage();
        }
    }

    public function delete($id){
        try{
            $employess = Employess::find($id);
            if(!$employess) {
                Sweetalert::info('Employee Not-found!', 'Info');
                return back();
            }

            $employess->delete();

            if($employess){
                Sweetalert::success('Employee deleted', 'Success');
                return back();
            }else{
                Sweetalert::error('Data cannot be deleted', 'Failed');
                return back();
            }
        }catch (\Exception $e){
            Log::error([
                "Message " . $e->getMessage(),
                "On file " . $e->getFile(),
                "On line " . $e->getLine(),
            ]);

            return $e->getMessage();
        }
    }

    public function detailsEmployess($id){
        try {
            $data = Employess::find($id);
            if (!$data) {
                Sweetalert::info('Employee Not-found', 'Info');
                return back();
            }

            return view('back.employess.details', compact('data'));
        }catch (\Exception $e){
            Log::error([
                "Message " . $e->getMessage(),
                "On file " . $e->getFile(),
                "On line " . $e->getLine(),
            ]);

            return $e->getMessage();
        }
    }

    public function searchEmployee(Request $request)
    {
        try {
            $response = $this->employeeService->search($request->all());
            return $response;
        }catch (\Exception $e){
            Log::error([
                "Message " . $e->getMessage(),
                "On file " . $e->getFile(),
                "On line " . $e->getLine(),
            ]);

            return $e->getMessage();
        }
    }

    public function importEmployee() {
        try{
            $validation = Validator::make(request()->all(), [
                'file'  =>  'required|mimes:xlsx, csv, xls'
            ]);

            if ($validation->fails()) {
                Sweetalert::error(implode($validation->errors()->all()), 'Failed');
                return back();
            }

            $response = $this->employeeService->import();
            return $response;
        }catch (\Exception $e){
            Log::error([
                "Message " . $e->getMessage(),
                "On file " . $e->getFile(),
                "On line " . $e->getLine(),
            ]);

            return $e->getMessage();
        }
    }
}
