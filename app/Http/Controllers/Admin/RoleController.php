<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Role;
use Illuminate\Http\Request;

class RoleController extends Controller
{
    public function index()
    {
        $role = Role::orderBy('created_at', 'desc')->paginate(10);
        return view('back.role.index', compact('role'));
    }
}
