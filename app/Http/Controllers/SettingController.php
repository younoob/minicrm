<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Database\Connection;
use DB;

class SettingController extends Controller
{
    public function check_connection()
    {
        try{
            DB::connection()->getPDO();
            return redirect()->back()->with("alert", "Connected successfuly to: " . DB::connection()->getDatabaseName());
        }catch (\Exception $e) {
            die("Could not connect to the database. Please check your configuration. error: " . $e);
        }
    }

    public function ip_location()
    {
        $ipLocation = geoip()->getLocation($_SERVER['REMOTE_ADDR']);
        return view('back.settings.ipLocation', compact('ipLocation'));
    }
}
