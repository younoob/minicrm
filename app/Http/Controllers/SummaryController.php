<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Summary;
use App\Models\Employee;
use App\Models\Sell;
use Redirect;
use Config;
use Session;
use DB;
use Carbon\Carbon;

class SummaryController extends Controller
{
    public function index()
    {
        $summary = Summary::paginate(10);
        return view('back.summary.index', compact('summary'));
    }

    public function indexDetails($id)
    {
        $summary = Summary::where('id', $id)->first();
        $sell = Sell::where('employee_id', $summary->employee_id)->whereDate('created_at',$summary->created_at)->orderBy('created_at', 'DESC')->paginate();

        return view('back.summary.details', compact('summary', 'sell', 'id'));
    }

    public function searchSummary(Request $request)
    {
        $keyword = $request->qsummary;
        $date = $request->date_range;

        $date = explode('-', trim(str_replace(' ', '', $request->date_range)));

        $date_range = $date[0].' - '.$date[1];

        $from = Carbon::parse($date[0]);
        $to = Carbon::parse($date[1]);

        if($date[0] == $date[1]){
            $summary = Summary::where(function($query) use($keyword){
                $query->where('price_total', 'like', "%$keyword%");
                $query->orWhere('discount_total', 'like', "%$keyword%");
                $query->orWhere('total', 'like', "%$keyword%");
                $query->orWhereDate('created_at', 'like', "%$keyword%");
                $query->orWhereHas('getEmployeeName', function($sub_query) use($keyword){
                    $sub_query->where('fisrt_name', 'like', "%$keyword%");
                    $sub_query->orWhereHas('getCompanyName', function($company) use($keyword){
                        $company->where('name', 'like', "%$keyword%");
                    });
                });
                $query->orWhereHas('getEmployeeName', function($sub_query_name) use($keyword){
                    $sub_query_name->where('last_name', 'like', "%$keyword%");
                    $sub_query_name->orWhereHas('getCompanyName', function($company) use($keyword){
                        $company->where('name', 'like', "%$keyword%");
                    });
                });
            })
            ->whereDate('created_at', [Carbon::parse($date[0])])->orderBy('created_at', 'desc')
            ->paginate();
        }else{
            $summary = Summary::where(function($query) use($keyword){
                $query->where('price_total', 'like', "%$keyword%");
                $query->orWhere('discount_total', 'like', "%$keyword%");
                $query->orWhere('total', 'like', "%$keyword%");
                $query->orWhereDate('created_at', 'like', "%$keyword%");
                $query->orWhereHas('getEmployeeName', function($sub_query) use($keyword){
                    $sub_query->where('fisrt_name', 'like', "%$keyword%");
                    $sub_query->orWhereHas('getCompanyName', function($company) use($keyword){
                        $company->where('name', 'like', "%$keyword%");
                    });
                });
                $query->orWhereHas('getEmployeeName', function($sub_query_name) use($keyword){
                    $sub_query_name->where('last_name', 'like', "%$keyword%");
                    $sub_query_name->orWhereHas('getCompanyName', function($company) use($keyword){
                        $company->where('name', 'like', "%$keyword%");
                    });
                });
            })
            ->whereBetween('created_at', [$from, $to])
            ->paginate(10);
        }

        return view('back.summary.search', compact('summary', 'date_range', 'date', 'keyword'));
    }

}
