<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Employess;
use App\Models\Sell;
use DB;

class SearchController extends Controller
{
    public function search(Request $request) 
    {
        $keyword = $request->q;

        $user = DB::table('users')
                ->where('name', 'like', "%$keyword%")
                ->orWhere('email', 'like', "%$keyword%")
                ->orWhere('role', 'like', "%$keyword%")
                ->orWhere('created_at', 'like', "%$keyword%")->paginate();

        $company = DB::table('companies')
                    ->where('name', 'like', "%$keyword%")
                    ->orWhere('email', 'like', "%$keyword%")
                    ->orWhere('website', 'like', "%$keyword%")
                    ->orWhere('created_at', 'like', "$keyword")
                    ->orwhere('created_by', 'like', "%$keyword%")
                    ->orWhere('updated_by', 'like', "%$keyword%")->paginate();

        $employess = Employess::where('fisrt_name', 'like', "%$keyword%")
                    ->orWhere('last_name', 'like', "%$keyword%")
                    ->orWhere('email', 'like', "%$keyword%")
                    ->orWhere('phone', 'like', "%$keyword%")
                    ->orWhereDate('created_at', 'like', "%$keyword%")
                    ->orWhereDate('created_by', 'like', "%$keyword%")
                    ->orWhereDate('updated_by', 'like', "%$keyword%")
                    ->orWhereHas('getCompanyName', function($query) use ($keyword){
                        $query->where('name', 'like', "%$keyword%");
                    })->paginate();

        $item = DB::table('items')
                ->where('name', 'like', "%$keyword%")
                ->orWhere('price', 'like', "%$keyword%")->paginate();

        $sell = Sell::where('price', 'like', "%$keyword%")
                    ->orWhere('discount', 'like', "%$keyword%")
                    ->orWhereHas('getItemName', function($item_query) use($keyword){
                        $item_query->where('name', 'like', "%$keyword%");
                    })->paginate();

        return view('back.search', ['user' => $user, 'company' => $company, 'employess' => $employess, 'sell' => $sell], 
        compact('user','company', 'employess', 'item', 'sell', 'keyword'));
    }
}
