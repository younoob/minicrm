<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use App\Models\User;
use App\Models\Company;
use App\Models\Employess;
use App\Models\Item;
use App\Models\Sell;
use App\Models\Summary;


class SystemController extends Controller
{

    public function index(){
        $employess = Employess::all();
        $company = Company::all();
        $user = User::all();
        $item = Item::all();
        $sell = Sell::all();
        $summary = Summary::all();
        return view('back.dashboard', compact('user','company','employess', 'item', 'sell', 'summary'));

    }


    public function user(){
        $users = User::orderBy('created_at', 'desc')->paginate(10);
        return view('back.user', compact('users'));
    }

    public function del_user($id) {
        $user = User::find($id)->delete();
        if ($user) {
            return redirect()->back()->with('delsuc','Deleted');
        }else {
            return redirect()->back()->with('delfal','Cannot Delete');
        }
    }



}
