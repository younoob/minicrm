<?php

namespace App\Services;

use App\Models\Item;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Session;
use Wavey\Sweetalert\Sweetalert;
use Illuminate\Support\Facades\Redirect;


/**
 * Class ItemService
 * @package App\Services
 */
class ItemService
{
    public function create($data)
    {
        Config::set('app.timezone', 'UTC');

        $existingItem = Item::where('name', $data['name'])->get();

        if (count($existingItem)>0) {
            Sweetalert::info('Items already exist', 'Info');
            return back();
        }

        $item = Item::create([
            'name' => $data['name'],
            'price' => $data['price'],
        ]);

        if($item){
            Config::set('app.timezone', Session::get('timezone'));
            Sweetalert::success('Item created!', 'Success');
            return Redirect::to('admin/item');
        }else{
            Sweetalert::error('Item cannot be created' ,'Failed');
            return back();
        }
    }

    public function update($data, $itemID)
    {
        $item = Item::find($itemID);

        if (!$item){
            Sweetalert::info('Item Not-found!', 'Info');
            return back();
        }

        Config::set('app.timezone', 'UTC');

        $item->update([
            'name' => $data['name'],
            'price' => $data['price'],
        ]);

        if($item){
            Config::set('app.timezone', Session::get('timezone'));
            Sweetalert::success('Data Updated', 'Success');
            return Redirect::to('/admin/item');
        }else{
            Sweetalert::error('Item cannot be updated!', 'Failed');
            return back();
        }
    }
}
