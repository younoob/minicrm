<?php

namespace App\Services;

use App\Imports\CompanyImport;
use App\Jobs\SendEmailCompany;
use App\Models\Company;
use Carbon\Carbon;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Wavey\Sweetalert\Sweetalert;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;

/**
 * Class CompanyService
 * @package App\Services
 */
class CompanyService
{
    public function create($data, $createdBy)
    {
        Config::set('app.timezone', 'UTC');

        if (!empty($data->email)){
            $emailData = (new SendEmailCompany($data->email))->delay(Carbon::now()->addSecond(5));
            dispatch($emailData);
        }

        if ($data['image']->isValid()) {
            $path = '/company/';
            $name = $path.substr(mt_rand(00000, 99999), 0, 5) . '.' . $data['image']->getClientOriginalExtension();

            $data['image']->move(public_path($path), $path.$name);

            Company::create([
                'name'          => $data['name'],
                'email'         => $data['email'],
                'image'         => $name,
                'website'       => $data['website'],
                'created_by'    => $createdBy,
            ]);
            Sweetalert::success('Data Created and Confirmation Email has Been Sent', 'Success');
            return Redirect::to('admin/company');
        }

        Config::set('app.timezone', Session::get('timezone'));
        Sweetalert::error('Something Else', 'Error');
        return redirect()->back();
    }

    public function update($data, $companyID, $updatedBy)
    {
        $company = Company::find($companyID);
        if(!$company) Sweetalert::error('Company Not-found' ,'Failed');

        Config::set('app.timezone', 'UTC');

        if (!empty($data['image'])) {
            if($data['image']->isValid()) {
                if (File::exists(public_path($company->image))) File::delete(public_path($company->image));

                $path = '/company/';
                $name = $path.substr(mt_rand(00000, 99999), 0, 5) . '.' . $data['image']->getClientOriginalExtension();

                $data['image']->move(public_path($path), $path.$name);

                $company->update([
                    'name' => $data['name'],
                    'email' => $data['email'],
                    'image' => $name,
                    'website' => $data['website'],
                    'updated_by' => $updatedBy,
                ]);

                Sweetalert::success('Data updated', 'Success');
                return Redirect::to('admin/company');
            }
        } else {
            $company->update([
                'name' => $data['name'],
                'email' => $data['email'],
                'image' => $company->image,
                'website' => $data['website'],
                'updated_by' => $updatedBy,
            ]);

            Sweetalert::success('Data updated with old image', 'Success');
            return Redirect::to('admin/company');
        }

        Config::set('app.timezone', Session::get('timezone'));
        Sweetalert::error('Data cannot be updated', 'Failed');
        return redirect()->back();
    }

    public function search($data)
    {
        $keyword = $data['qcompany'];
        $date = $data['date_range'];

        $date = explode('-', trim(str_replace(' ', '', $data['date_range'])));

        $date_range = $date[0].' - '.$date[1];

        $from = Carbon::parse($date[0]);
        $to = Carbon::parse($date[1]);

        if($date[0] == $date[1]){
            $companies = DB::table('companies')
                    ->where(function($query) use($keyword){
                        $query->where('name', 'like', "%$keyword%");
                        $query->orWhere('email', 'like', "%$keyword%");
                        $query->orWhere('website', 'like', "%$keyword%");
                        $query->orWhereDate('created_at', 'like', "%$keyword%");
                        $query->orwhereDate('created_by', 'like', "%$keyword%");
                        $query->orWhereDate('updated_by', 'like', "%$keyword%");
                    })
                    ->whereDate('created_at', [Carbon::parse($date[0])])->orderBy('created_at', 'desc')
                    ->paginate();
        }else{
            $companies = DB::table('companies')
                    ->where(function($query) use($keyword){
                        $query->where('name', 'like', "%$keyword%");
                        $query->orWhere('email', 'like', "%$keyword%");
                        $query->orWhere('website', 'like', "%$keyword%");
                        $query->orWhereDate('created_at', 'like', "%$$keyword%");
                        $query->orwhereDate('created_by', 'like', "%$keyword%");
                        $query->orWhereDate('updated_by', 'like', "%$keyword%");
                    })
                    ->whereBetween('created_at', [$from, $to])
                    ->paginate();
        }

        return view('back.company.search', ['companies' => $companies], compact('companies', 'date_range', 'keyword', 'date'));
    }

    public function import()
    {
        $importResult = Excel::toArray(new CompanyImport, request()->file('file'));
        $importedCount = 0;
        $skippedCount = 0;

        foreach ($importResult[0] as $row) {
            $importedCompany = new CompanyImport($row);
            $companyModel = $importedCompany->model($row);

            if ($companyModel) {
                try {
                    $companyModel->save();
                    $importedCount++;
                } catch (\Exception $e) {
                    $errorMessages[] = $e->getMessage();
                    $skippedCount++;
                }
            } else {
                $skippedCount++;
            }
        }

        Sweetalert::success('Data Imported: ' . $importedCount . ' records imported, ' . $skippedCount . ' records skipped', 'Success');
        return redirect('admin/company');
    }
}
