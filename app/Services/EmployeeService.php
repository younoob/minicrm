<?php

namespace App\Services;

use App\Models\Employess;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Redirect;
use Wavey\Sweetalert\Sweetalert;
use Maatwebsite\Excel\Facades\Excel;
use App\Imports\EmployessImport;
use Carbon\Carbon;


/**
 * Class EmployeeService
 * @package App\Services
 */
class EmployeeService
{
    public function create($data, $createdBy)
    {
        Config::set('app.timezone', 'UTC');

        $employess = Employess::create([
            'fisrt_name' => $data['fisrt_name'],
            'last_name' => $data['last_name'],
            'company_id' => $data['company_id'],
            'email' => $data['email'],
            'phone' => $data['phone'],
            'password' => bcrypt($data['password']),
            'created_by' => $createdBy,
        ]);

        if($employess){
            Config::set('app.timezone', Session::get('timezone'));
            Sweetalert::success('Employee registered', 'Success');
            return Redirect::to('admin/employess');
        }else{
            Sweetalert::error('Employee cannot be registered!', 'Failed');
            return back();
        }
    }

    public function update($data, $employeeID, $updatedBy)
    {
        $employess = Employess::find($employeeID);

        if(!$employess) {
            Sweetalert::info('Employee Not-found!', 'Info');
            return back();
        }

        $existingEmail = Employess::where('email', $data['email'])
                        ->where('id', '!=', $employeeID)
                        ->count();

        if($existingEmail > 0) {
            Sweetalert::info('Email has been used!', 'Info');
            return back();
        }

        Config::set('app.timezone', 'UTC');

        $updateData = [
            'fisrt_name' => $data['fisrt_name'],
            'last_name' => $data['last_name'],
            'company_id' => $data['company_id'],
            'email' => $data['email'],
            'phone' => $data['phone'],
            'password' => bcrypt($data['password']),
            'updated_by' => $updatedBy,
        ];

        if (!empty($data['password'])) {
            $updateData['password'] = bcrypt($data['password']);
        }

        $employess->update($updateData);

        if($employess){
            Config::set('app.timezone', Session::get('timezone'));
            Sweetalert::success('Data employee has been updated', 'Success');
            return Redirect::to('admin/employess');
        }else{
            Sweetalert::error('Data employee cannot be updated', 'Failed');
            return back();
        }
    }

    public function search($data)
    {
        $keyword = $data['qemployee'];
        $date = $data['date_range'];

        $date = explode('-', trim(str_replace(' ', '', $data['date_range'])));

        $date_range = $date[0].' - '.$date[1];

        $from = Carbon::parse($date[0]);
        $to = Carbon::parse($date[1]);

        if($date[0] == $date[1]){
            $employee = Employess::where(function($query) use($keyword){
                $query->where('fisrt_name', 'like', "%$keyword%");
                $query->orWhere('last_name', 'like', "%$keyword%");
                $query->orWhere('email', 'like', "%$keyword%");
                $query->orWhere('phone', 'like', "%$keyword%");
                $query->orWhereDate('created_at', 'like', "%$keyword%");
                $query->orWhereHas('getCompanyName', function($sub_query) use($keyword){
                    $sub_query->where('name', 'like', "%$keyword%");
                });
            })
            ->whereDate('created_at', [Carbon::parse($date[0])])->orderBy('created_at','desc')
            ->paginate();
        }else{
            $employee = Employess::where(function($query) use($keyword){
                $query->where('fisrt_name', 'like', "%$keyword%");
                $query->orWhere('last_name', 'like', "%$keyword%");
                $query->orWhere('email', 'like', "%$keyword%");
                $query->orWhere('phone', 'like', "%$keyword%");
                $query->orWhereDate('created_at', 'like', "%$keyword%");
                $query->orWhereHas('getCompanyName', function($sub_query) use($keyword){
                    $sub_query->where('name', 'like', "%$keyword%");
                });
            })
            ->whereBetween('created_at', [$from, $to])
            ->paginate();
        }

        return view('back.employess.search', ['employee' => $employee], compact('employee', 'date_range', 'keyword', 'date'));
    }

    public function import()
    {
        $importResult = Excel::toArray(new EmployessImport, request()->file('file'));
        $importedCount = 0;
        $skippedCount = 0;

        foreach ($importResult[0] as $row) {
            $importedCompany = new EmployessImport($row);
            $companyModel = $importedCompany->model($row);

            if ($companyModel) {
                try {
                    $companyModel->save();
                    $importedCount++;
                } catch (\Exception $e) {
                    $errorMessages[] = $e->getMessage();
                    $skippedCount++;
                }
            } else {
                $skippedCount++;
            }
        }

        Sweetalert::success('Data Imported: ' . $importedCount . ' records imported, ' . $skippedCount . ' records skipped', 'Success');
        return back();
    }
}
