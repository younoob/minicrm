<?php

namespace App\Imports;

use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithCustomCsvSettings;
use Maatwebsite\Excel\Concerns\WithStartRow;

use App\Models\Company;

class CompanyImport implements ToModel, WithHeadingRow,  WithStartRow, WithCustomCsvSettings
{
    use Importable;
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model/null
    *
    */

    public function startRow(): int
    {
        return 2;
    }

    public function getCsvSettings(): array
    {
        return [
            'delimiter' => ';'
        ];
    }

    public function model(array $row)
    {
        $existingCompany = Company::where('email', $row['email'])->first();

        if ($existingCompany) {
            return null;
        }

        return new Company([
            'name' => $row['name'],
            'email' => $row['email'],
        ]);
    }
}
