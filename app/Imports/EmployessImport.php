<?php

namespace App\Imports;

use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use App\Models\Employess;

class EmployessImport implements ToModel, WithHeadingRow
{
    use Importable;
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model/null
    *
    */

    public function model(array $row)
    {
        $existingEmployee = Employess::where('email', $row['email'])->first();

        if ($existingEmployee) {
            return null;
        }

        return new Employess([
            'fisrt_name' => $row['fisrt_name'],
            'last_name' => $row['last_name'],
            'company_id' => $row['company_id'],
            'email' => $row['email'],
            'phone' => $row['phone'],
        ]);
    }
}
