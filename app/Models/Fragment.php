<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;


class Fragment extends Model
{
    use HasFactory; 

    protected $table = 'fragments';
    protected $primaryKey = 'key';
    protected $fillable = ['id', 'jp'];

    public $incrementing = false;
}
