<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Employess extends Model
{
    use HasFactory;
    
    protected $table = 'employesses';
    protected $fillable = [
        'fisrt_name', 'last_name', 'company_id', 'email', 'phone', 'password', 'created_by', 'updated_by',
    ]; 

    public function getCompanyName() {
        return $this->belongsTo('App\Models\Company', 'company_id');
    }

    public function getCreatedName() {
        return $this->belongsTo('App\Models\User', 'created_by');
    }

    public function getUpdatedName() {
        return $this->belongsTo('App\Models\User', 'updated_by');
    }
}
