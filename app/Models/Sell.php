<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Sell extends Model
{
    use HasFactory;

    protected $table = "sells";
    protected $primaryKey = 'id';
    protected $fillable = [
        'item_id', 'price', 'discount', 'employee_id',
    ];

    public function getItemName() {
        return $this->belongsTo('App\Models\Item', 'item_id', 'id');
    }

    public function getEmployeeName() {
        return $this->belongsTo('App\Models\Employess', 'employee_id');
    }
}
