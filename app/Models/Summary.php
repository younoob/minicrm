<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Summary extends Model
{
    use HasFactory;

    protected $table = "summaries";
    protected $fillable = ['employee_id', 'price_total', 'discount_total', 'total'];

    public function getEmployeeName()
    {
        return $this->belongsTo('App\Models\Employess', 'employee_id', 'id');
    }

}
