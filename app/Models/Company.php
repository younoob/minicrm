<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
    use HasFactory;

    protected $table = 'companies';
    protected $fillable = [
        'name', 'email', 'image', 'website', 'created_by', 'updated_by'
    ];

    public function company(){
        return $this->hasMany('App\Models\Employess','company_id');
    }

    public function getCreatedName(){
        return $this->belongsTo('App\Models\User', 'created_by');
    }

    public function getUpdatedName(){
        return $this->belongsTo('App\Models\User', 'updated_by');
    }
}
