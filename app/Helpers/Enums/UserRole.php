<?php
namespace App\Helpers\Enums;

final class UserRole {
    const ADMIN_HRD = 1;
    const EMPLOYEE = 2;

    const ROLE_LIST = [
        'Admin HRD',
        'Employee'
    ];

    public static function getName ($role) {
        return self::ROLE_LIST[$role];
    }
}
