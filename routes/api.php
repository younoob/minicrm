<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\JWTController;
use App\Http\Controllers\DataController;
use App\Http\Controllers\APIController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('register', [JWTController::class, 'register']);
Route::post('login', [JWTController::class, 'login']);
Route::get('logout', [JWTController::class, 'logout']);
Route::get('user', [JWTController::class, 'user']);

// Route::group(['middleware' => ['jwt.verify']], function() {
//     Route::get('user', [JWTController::class ,'getAuthenticatedUser']);
//     Route::get('closed', [DataController::class ,'closed']);
// });

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::middleware(['api', 'data.verify'])->group(function($router){
    Route::get('employess/data/{id}', [APIController::class, 'data_employess']);
});
