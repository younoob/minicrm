<?php

use App\Http\Controllers\Admin\RoleController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\SystemController;
use App\Http\Controllers\AuthenticationController;
use App\Http\Controllers\SettingController;
use App\Http\Controllers\SearchController;
use App\Http\Controllers\EmployeeController;
use App\Http\Controllers\CompanyController;
use App\Http\Controllers\ItemController;
use App\Http\Controllers\SellController;
use App\Http\Controllers\SummaryController;

Route::get('/', [AuthenticationController::class, 'view']);
Route::post('/login', [AuthenticationController::class, 'loginproccess']);
Route::get('/logout', [AuthenticationController::class, 'logout']);
Route::get('/register', [AuthenticationController::class, 'register']);
Route::post('/register', [AuthenticationController::class, 'register_on']);

// Unauthorized page
Route::prefix('unauthorized')->group(function () {
    Route::get('not-found', function () {
        return view('unauthorized.404');
    });
});


// Admin Wokspace Route
Route::group(['prefix' => '/admin', 'middleware' => ['is_admin']], function() {
    Route::get('/', [SystemController::class, 'index']);
    Route::get('/users', [SystemController::class, 'user']);
    Route::get('/delete_user/{id}', [SystemController::class, 'del_user']);
    Route::post('/search', [SearchController::class, 'search']);

    Route::group(['prefix' => 'company'], function() {
        Route::get('/', [CompanyController::class, 'company']);
        Route::get('/create', [CompanyController::class, 'indexCreate']);
        Route::post('/create', [CompanyController::class, 'createCompany']);
        Route::get('/update/{id}', [CompanyController::class, 'updateView']);
        Route::post('/update/{id}', [CompanyController::class, 'updateCompany']);
        Route::get('/delete/{id}', [CompanyController::class, 'deleteCompany']);
        Route::get('/details/{id}', [CompanyController::class, 'detailsCompany']);
        Route::post('/search', [CompanyController::class, 'searchCompany']);
        Route::post('/import', [CompanyController::class, 'importCompany']);
    });

    Route::group(['prefix' => 'employess'], function() {
        Route::get('/', [EmployeeController::class, 'employessView']);
        Route::get('/create', [EmployeeController::class, 'indexemployess']);
        Route::post('/create', [EmployeeController::class, 'createEmployess']);
        Route::get('/update/{id}', [EmployeeController::class, 'indexUpdateEmployess']);
        Route::post('/update/{id}', [EmployeeController::class, 'updateEmployess']);
        Route::get('/delete/{id}', [EmployeeController::class, 'delete']);
        Route::get('/details/{id}', [EmployeeController::class, 'detailsEmployess']);
        Route::post('/search', [EmployeeController::class, 'searchEmployee']);
        Route::post('/import', [EmployeeController::class, 'importEmployee']);
    });

    Route::group(['prefix' => 'item'], function() {
        Route::get('/', [ItemController::class, 'index']);
        Route::get('/create', [ItemController::class, 'indexCreate']);
        Route::post('/create', [ItemController::class, 'createData']);
        Route::get('/update/{id}', [ItemController::class, 'indexUpdate']);
        Route::post('/update/{id}', [ItemController::class, 'updateData']);
        Route::get('/delete/{id}', [ItemController::class, 'deleteData']);
        Route::post('/search', [ItemController::class, 'searchItem']);
    });

    Route::group(['prefix' => 'sell'], function() {
        Route::get('/', [SellController::class, 'index']);
        Route::get('/create', [SellController::class, 'indexCreate']);
        Route::post('/create', [SellController::class, 'createSell']);
        Route::get('/update/{id}', [SellController::class, 'indexUpdate']);
        Route::post('/update/{id}', [SellController::class, 'updateSell']);
        Route::get('/delete/{id}', [SellController::class, 'deleteSell']);
        Route::post('/search', [SellController::class, 'searchSell']);
    });

    Route::group(['prefix' => 'summary'], function() {
        Route::get('/', [SummaryController::class, 'index']);
        Route::get('/detail/{id}',[SummaryController::class, 'indexDetails']);
        Route::post('/search', [SummaryController::class, 'searchSummary']);
    });

    Route::group(['prefix' => 'setting'], function() {
        Route::get('check-connection', [SettingController::class, 'check_connection']);
        Route::get('ipLocation', [SettingController::class, 'ip_location']);
    });

    Route::group(['prefix' => 'role'], function() {
        Route::get('/', [RoleController::class, 'index']);
    });
});
