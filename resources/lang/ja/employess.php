<?php

return [
    'title' => '従誤飲のデータ',
    'table' => [
        'number' => '番後',
        'name' => '名前',
        'company' => '会社',
        'email' => 'メール',
        'password' => 'パスワード',
        'phone_number' => '電話番後',
        'joined' => '参加',
        'action' => 'アクション',
        'edit' => '更新',
        'delete' => '削除',
        'created_by' => 'によって作成された',
        'updated_by' => 'によって更新された',
    ],
    'create' => '作り',
    'create_data' => [
        'first_name' => 'ファーストネーム',
        'last_name' => '苗字',
        'title' => '従誤飲のデータを作る',
        'submit' => '作り',
    ],
    'update_data' => [
        'title' => '従誤飲のデータを更新する',
        'update' => '更新'
    ],
];

?>