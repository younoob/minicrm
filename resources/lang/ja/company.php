<?php

return [
    'title' => '会社のデータ',
    'table' => [
        'number' => '番後',
        'name' => '名前',
        'email' => 'メール',
        'logo' => '写真',
        'website' => 'ウェブサイト',
        'joined' => '参加',
        'action' => 'アクション',
        'edit' => '更新',
        'delete' => '削除',
        'created_by' => 'によって作成された',
        'updated_by' => 'によって更新された',
    ],
    'create' => '作り',
    'create_data' => [
        'title' => '会社のデータを作る',
        'submit' => '作り',
    ],
    'update_data' => [
        'title' => '会社のデータを更新する',
        'update' => '更新'
    ],
    
];

?>