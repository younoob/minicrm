<?php

return [
    'title' => 'MiniCRM',
    'intruction' => 'Sign in to start your session',
    'show_pass' => 'Show Password',
    'btn_signin' => 'Sign In',
    'social_media' => [
        'in' => 'Sign in using Linkedin',
        'git' => 'Sign in using Github',
    ],
    'register' => 'Register a new membership',
];

?>