<?php

return [
    'title' => 'MiniCRM',
    'intruction' => 'Masuk untuk memulai sesi',
    'show_pass' => 'Tampilkan kata sandi',
    'btn_signin' => 'Masuk',
    'social_media' => [
        'in' => 'Masuk menggunakan Linkedin',
        'git' => 'Masuk menggunakan Github',
    ],
    'register' => 'Pendaftaran anggota baru',
];

?>