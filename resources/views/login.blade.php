<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>MiniCRM | Log in</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- Font Awesome -->
  <link rel="stylesheet" href="{{asset('style/plugins/fontawesome-free/css/all.min.css')}}">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- icheck bootstrap -->
  <link rel="stylesheet" href="{{asset('style/plugins/icheck-bootstrap/icheck-bootstrap.min.css')}}">
  <!-- Theme style -->
  <link rel="stylesheet" href="{{asset('style/dist/css/adminlte.min.css')}}">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
  <style>
    .fa{
      cursor: pointer;
    }
  </style>
   @notifyCss
</head>
<body class="hold-transition login-page">
<div class="login-box">
  <div class="login-logo">
    <a href="#"><b>Mini</b>CRM</a>
  </div>
  <!-- /.login-logo -->
  <div class="card">
    <div class="card-body login-card-body">
      <p class="login-box-msg">@lang('login.intruction')</p>

      @if(session()->has('failed'))
          <div class="alert alert-warning text-white">
            <span>{{session('failed')}} <i class="fa fa-times float-right" data-dismiss="alert"></i></span>
          </div>
      @endif

      @if(session()->has('success'))
        <div class="alert alert-danger text-white">
          <span>{{session('success')}} <i class="fa fa-times float-right" data-dismiss="alert"></i></span>
        </div>
      @endif

      <form action="{{url('/login')}}" method="post">
      {{csrf_field()}}

        <div class="input-group mt-2">
          <input type="email" name="email" class="form-control" placeholder="Email">
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-envelope"></span>
            </div>
          </div>
        </div>
        @error('email')
            <span class="text-danger text-sm mb-3">{{$message}}</span>
        @enderror

        <div class="input-group mt-2">
          <input type="password" id="password" name="password" class="form-control" placeholder="Password">
          <div class="input-group-append">
              <div class="input-group-text">
                  <span class="fas fa-lock"></span>
                </div>
            </div>
        </div>
        @error('password')
          <span class="text-danger text-sm">{{$message}}</span>
        @enderror

        <div class="row mt-3">
          <div class="col-8">
            <div class="icheck-primary">
              <input type="checkbox" id="remember" onclick="showPassword()">
              <label for="remember">
                @lang('login.show_pass')
              </label>
            </div>
          </div>
          <!-- /.col -->
          <div class="col-4">
            <button type="submit" class="btn btn-primary btn-block">@lang('login.btn_signin')</button>
          </div>
          <!-- /.col -->
        </div>
      </form>

      <div class="col-12 row mt-3">
          <div class="md-4">
            @if(count(config('app.available_locales')) > 1)
                <a class="btn btn-info" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
                {{__('Lang')}} : {{ strtoupper(app()->getLocale()) }}
                </a>

              <div class="dropdown-menu dropdown-menu-right">
                @foreach(config('app.available_locales') as $langLocale => $langName)
                  <a href="{{ url()->current() }}?change_language={{$langLocale}}" class="dropdown-item">
                    {{strtoupper($langLocale)}} ({{$langName}})
                  </a>
                @endforeach
              </div>
            @endif
          </div>

          <div class="md-4 text-sm m-auto text-right">
            <a href="{{url('/register')}}">@lang('login.register')</a>
          </div>

      </div>
    </div>
    <!-- /.login-card-body -->
  </div>
</div>
<!-- /.login-box -->

<!-- Custom Show Password -->
<script>
  function showPassword() {
    var x = document.getElementById("password");
    if (x.type === "password"){
      x.type = "text";
    }else{
      x.type = "password";
    }
  }
</script>
<!-- End Custom Show Password -->

<!-- jQuery -->
<script src="{{asset('style/plugins/jquery/jquery.min.js')}}"></script>
<!-- Bootstrap 4 -->
<script src="{{asset('style/plugins/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
<!-- AdminLTE App -->
<script src="{{asset('style/dist/js/adminlte.min.js')}}"></script>

<x:notify-messages />
@notifyJs
</body>
</html>
