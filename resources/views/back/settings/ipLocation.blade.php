@extends('back.index')

@section('content')

<div class="container card card-body mt-3">
    <table class="table table-dark text-white">
        <thead>
            <tr>
                <th>Country</th>
                <th>City</th>
                <th>IP Adress</th>
            </tr>
        </thead>
        <tbody style="color: forestgreen;">
            <tr>
                <td>{{ $ipLocation->country }}</td>
                <td>{{ $ipLocation->city }}</td>
                <td>{{ $ipLocation->ip }}</td>
            </tr>
        </tbody>
    </table>
</div>

@stop
