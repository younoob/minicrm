@extends('back.index')

@section('content')

<section class="content">
      <div class="row">
        <div class="col-12">
         
          <div class="card">
            <div class="card-header">
              <h3 class="card-title">@lang('text.Data Users')</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">

                @if(session()->has('delsuc'))
                    <div class="alert alert-info" data-dismiss="alert">
                      <span>{{session('delsuc')}} <i class="fa fa-times float-right" data-dismiss="alert"></i></span>
                    </div>
                @endif

                @if(session()->has('delfal'))
                  <div class="alert alert-danger" data-dismiss="alert">
                    <span>{{session('delfal')}} <i class="fa fa-times float-right" data-dismiss="alert"></i></span>
                  </div>
                @endif

              <table class="table table-bordered table-striped">
                <thead class="text-center">
                <tr>
                  <th>@lang('text.No')</th>
                  <th>@lang('text.Name')</th>
                  <th>@lang('text.Email')</th>
                  <th>@lang('text.Role')</th>
                  <th>@lang('text.Joined')</th>
                  <th>@lang('text.Action')</th>
                </tr>
                </thead>
                <tbody>
                  @foreach($users as $index => $data)
                  <tr>
                    <td>{{++$index}}</td>
                    <td>{{$data->name}}</td>
                    <td>
                      <a href="mailto:{{$data->email}} ">{{$data->email}} </a>
                    </td>
                    <td>
                      <span class="badge badge-info">{{$data->getRoleName ? $data->getRoleName->name : 'not found'}}</span>
                    </td>
                    <td class="text-center">{{ \Carbon\Carbon::parse($data->created_at)->setTimezone(Session::get('timezone')) }}</td>
                    <td>
                      <a href="{{url('admin/delete_user/'.$data->id)}}" class="btn btn-danger"><i class="fa fa-trash"></i></a>
                    </td>
                  </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
            
            <div class="d-flex justify-content-center">
                {{ $users->links() }}
            </div>

          </div>
        </div>
      </div>
    </section>

@stop