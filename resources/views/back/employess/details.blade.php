@extends('back.index')

@section('content')

 <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>@lang('text.Details')</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{ url('admin') }}">@lang('text.Home')</a></li>
              <li class="breadcrumb-item active">@lang('text.Details')</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">@lang('text.Employess Details')</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <div class="row">
                  <div class="col-sm-4">
                    <div class="position-relative py-3">
                    <img src="{{asset('style/dist/img/profile.png')}}" class="img-fluid img-circle shadow" width="500px" alt="Employess Image">
                    </div>
                  </div>
                  <div class="col-sm-8 float-right">
                      <div class="text-center">
                        <h1><strong>{{$data->fisrt_name}} {{$data->last_name}}</strong></h1>
                      </div>
                      <div class="py-6">
                          <div class="col-lg-12">
                            <ul>
                              <table>
                                <tr>
                                  <th>@lang('text.Company')</th>
                                  <th width="10">:</th>
                                  <td>{{$data->getCompanyName ? $data->getCompanyName->name:'data not found'}}</td>
                                </tr>
                                <tr>
                                  <th>@lang('text.Email')</th>
                                  <th width="10">:</th>
                                  <td><a href="mailto:{{$data->email}}">{{$data->email}}</a></td>
                                </tr>
                                <tr>
                                  <th>@lang('text.Phone Number')</th>
                                  <th width="10">:</th>
                                  <td><a href="tel:{{$data->phone}}">{{$data->phone}}</a></td>
                                </tr>
                                <tr>
                                  <th>@lang('text.Joined')</th>
                                  <th width="10">:</th>
                                  <td>{{ \Carbon\Carbon::parse($data->created_at)->setTimezone(Session::get('timezone')) }}</td>
                                </tr>
                                <tr>
                                  <th>@lang('text.Created By')</th>
                                  <th width="10">:</th>
                                  <td width="150">{{ \Carbon\Carbon::parse($data->created_at)->setTimezone(Session::get('timezone')) }}</td>
                                  <th>({{$data->getCreatedName ? $data->getCreatedName->name: 'uknown'}})</th>
                                </tr>
                                <tr>
                                  <th>@lang('text.Updated By')</th>
                                  <th width="10">:</th>
                                  <td width="150">{{ \Carbon\Carbon::parse($data->updated_at)->setTimezone(Session::get('timezone')) }}</td>
                                  <th>({{$data->getUpdatedName ? $data->getUpdatedName->name: 'uknown'}})</th>
                                </tr> 
                              </table>
                            </ul>
                          </div>
                          <br>
                          <br>
                          <br>
                          <br>
                          <a href="{{ url('admin/employess') }}" class="btn btn-warning">@lang('text.Back')</a>
                      </div>
                  </div>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->

@stop