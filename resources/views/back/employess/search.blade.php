@extends('back.index')

@section('content')

<section class="content">
    <div class="raw">
        <div class="col-12">

            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">@lang('text.Data Employess')</h3>

                    <form action="{{ url('admin/employess/search') }}" method="post" role="form" class="form-inline ml-3 float-right">
                        {{ csrf_field() }}

                        <div class="row">
                            <div class="col-sm-5">
                                <div class="input-group input-group-sm">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">
                                            <i class="far fa-calender-alt"></i>
                                        </span>
                                    </div>
                                    <input type="text" name="date_range" class="form-control float-right" id="reservation">
                                </div>
                            </div>

                            <div class="col-sm-5">
                                <div class="input-group input-group-sm">
                                    <input type="text" name="qemployee" placeholder="Search Employee" aria-label="Search employee">
                                </div>
                            </div>

                            <div class="col-sm-2">
                                <button class="btn btn-navbar" type="submit">
                                    <i class="fas fa-search"></i>
                                </button>
                            </div>
                        </div>
                    </form>

                </div>

            <div class="card-body">
                <table class="table table-bordered table-striped">
                    <thead class="text-center">
                        <tr>
                            <th>@lang('text.No')</th>
                            <th>@lang('text.Name')</th>
                            <th>@lang('text.Company')</th>
                            <th>@lang('text.Email')</th>
                            <th>@lang('text.Phone Number')</th>
                            <th>@lang('text.Joined')</th>
                            <th>@lang('text.Action')</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if($keyword || $date)
                            @forelse($employee as $index => $data)
                            <tr>
                                <td>{{++$index}}</td>
                                <td>{{ $data->fisrt_name }} {{ $data->last_name }}</td>
                                <td>{{ $data->getCompanyName ? $data->getCompanyName->name: 'notfound' }}</td>
                                <td><a href="mailto:{{$data->email}}"> {{ $data->email }} </a></td>
                                <td><a href="cal:{{$data->phone}}"> {{ $data->phone }} </a></td>
                                <td>{{ \Carbon\Carbon::parse($data->created_at)->setTimezone(Session::get('timezone')) }}</td>
                                <td></td>
                            </tr>
                            @empty
                            <tr>
                                <td class="text-center" colspan="7">No data found</td>
                            </tr>
                            @endforelse
                        @else
                        <tr>
                            <td class="text-center" colspan="7">Please input a keyword</td>
                        </tr>
                        @endif
                    </tbody>
                </table>
            </div>

            <div class="d-flex justify-content-center">
                {{ $employee->links() }}
            </div>

            <div class="card-footer row">
              <a href="{{url('/admin/employess')}}" class="btn btn-block btn-info btn-sm col-md-2">@lang('text.Back')</a>
            </div>
        </div>
    </div>
</section>

@stop