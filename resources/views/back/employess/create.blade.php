@extends('back.index')

@section('content')

<!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <!-- left column -->
          <div class="col-md-12">
            <!-- general form elements -->
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">@lang('text.Create Data Employess')</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form role="form" method="post" action="{{url('admin/employess/create')}}">
              {{csrf_field()}}

                <div class="card-body">
                <div class="row">
                 <div class="form-group col-md-6">
                    <label for="exampleInputFirstName1">@lang('text.First Name')</label>
                    <input type="text" name="fisrt_name" class="form-control" id="exampleInputFirstName1" placeholder="First Name">
                    @error('fisrt_name')
                        <span class="text-danger text-sm">{{$message}}</span>
                    @enderror
                  </div>
                  <div class="form-group col-md-6">
                    <label for="exampleInputLastName1">@lang('text.Last Name')</label>
                    <input type="text" name="last_name" class="form-control" id="exampleInputLastName1" placeholder="Last Name">
                    @error('last_name')
                        <span class="text-danger text-sm">{{$message}}</span>
                    @enderror
                  </div>
                </div>
                <div class="row">
                  <div class="form-group col-md-6">
                    <label for="exampleInputCompany">@lang('text.Company')</label>
                    <select class="form-control" name="company_id">
                    <option selected disabled>@lang('text.Company')</option>
                    @foreach($company as $data)
                        <option value="{{$data->id}}">{{$data->name}}</option>
                    @endforeach
                    </select>
                    @error('company_id')
                        <span class="text-danger text-sm">{{$message}}</span>
                    @enderror
                  </div>
                  <div class="form-group col-md-6">
                    <label for="exampleInputPhoneNumber1">@lang('text.Phone Number')</label>
                    <input type="text" name="phone" class="form-control" id="exampleInputPhoneNumber1" placeholder="Phone Number" id="phone_number" minlength="11" maxlength="15">
                    @error('phone')
                        <span class="text-danger text-sm">{{$message}}</span>
                    @enderror
                  </div>
                </div>
                <div class="row">
                  <div class="form-group col-md-6">
                    <label for="exampleInputEmail1">@lang('text.Email')</label>
                    <input type="email" name="email" class="form-control" id="exampleInputEmail1" placeholder="Email">
                    @error('email')
                        <span class="text-danger text-sm">{{$message}}</span>
                    @enderror
                  </div>
                  <div class="form-group col-md-6">
                    <label for="exampleInputPassword1">@lang('text.Password')</label>
                    <input type="password" name="password" class="form-control" id="exampleInputPassword1" placeholder="Password">
                    @error('email')
                        <span class="text-danger text-sm">{{$message}}</span>
                    @enderror
                  </div>
                </div>
                <!-- Hidden -->
                  <div class="form-group col-md-6" hidden>
                    <label for="exampleInputUserID1">User Id</label>
                    @guest
                    @else
                    <input type="text" name="created_by" class="form-control" id="exampleInputUserID1" placeholder="User ID" value="{{Auth::user()->id}}">
                    @endguest
                  </div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">@lang('text.Create')</button>
                  <a href="{{ url('admin/employess') }}" class="btn btn-warning">@lang('text.Back')</a>
                </div>
              </form>
            </div>
            <!-- /.card -->

          </div>
          <!--/.col (left) -->
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->

@stop

