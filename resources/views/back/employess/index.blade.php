@extends('back.index')

@section('content')

<section class="content">
      <div class="row">
        <div class="col-12">

          <div class="card">
            <div class="card-header">
              <h3 class="card-title">@lang('text.Data Employess')</h3>

              <form action="{{ url('admin/employess/search') }}" class="form-inline ml-3 float-right" role="form" method="post">
                {{csrf_field()}}

                <div class="row">
                  <div class="col-sm-5">
                    <div class="input-group input-group-sm">
                        <div class="input-group-prepend">
                          <span class="input-group-text">
                            <i class="far fa-calendar-alt"></i>
                          </span>
                        </div>
                        <input type="text" class="form-control float-right" name="date_range" id="reservation">
                    </div>
                  </div>

                  <div class="col-sm-5">
                    <div class="input-group input-group-sm">
                      <input type="text" class="form-control" type="search" placeholder="Search Employee" aria-label="Search Employee" name="qemployee">
                    </div>
                  </div>

                  <div class="col-sm-2">
                    <button class="btn btn-navbar" type="submit">
                      <i class="fas fa-search"></i>
                    </button>
                  </div>
                </div>
              </form>
            </div>
            <!-- /.card-header -->
            <div class="card-body">

              <table class="table table-bordered table-striped">
                <thead class="text-center">
                <tr>
                  <th>@lang('text.No')</th>
                  <th>@lang('text.Name')</th>
                  <th>@lang('text.Company')</th>
                  <th>@lang('text.Email')</th>
                  <th>@lang('text.Phone Number')</th>
                  <th>@lang('text.Joined')</th>
                  <th>@lang('text.Action')</th>
                </tr>
                </thead>
                <tbody>
                  @forelse($employess as $index => $data)
                  <tr>
                    <td>{{++$index}}</td>
                    <td>{{$data->fisrt_name}} {{$data->last_name}}</td>
                    <td>{{$data->getCompanyName ? $data->getCompanyName->name:'data not found'}}</td>
                    <td><a href="mailto:{{$data->email}}">{{$data->email}}</a></td>
                    <td><a href="tel:{{$data->phone}}">{{$data->phone}}</a></td>
                    <td>{{ \Carbon\Carbon::parse($data->created_at)->setTimezone(Session::get('timezone')) }}</td>
                    <td>
                      <a href="{{url('/admin/employess/update/'.$data->id)}}" class="text-warning action-element" title="edit"><i class="fa fa-edit"></i></a>
                      <a href="{{url('/admin/employess/delete/'.$data->id)}}" class="text-danger action-element" title="delete"><i class="fa fa-trash"></i></a>
                      <a href="{{url('/admin/employess/details/'.$data->id)}}" class="text-info modal-info action-element" title="details">
                        <i class="fas fa-info-circle"></i>
                      </a>
                    </td>
                  </tr>
                  @empty
                  <tr>
                    <td colspan="7" class="text-center">No data found.</td>
                  </tr>
                  @endforelse
                </tbody>
              </table>
            </div>

            <div class="d-flex justify-content-center">
              {{ $employess->links() }}
            </div>

            <div class="card-footer row">
              <a href="{{url('/admin/employess/create')}}" class="btn btn-block btn-info btn-sm col-md-2">@lang('text.Create')</a>
              <button type="button" class="btn btn-success mx-3" data-toggle="modal" data-target="#modal-lg">
                Import Employee
              </button>
            </div>
          </div>
        </div>
      </div>
    </section>

    <div class="modal fade" id="modal-lg">
        <div class="modal-dialog modal-lg">
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title">Form Import Employee</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <form role="form" method="post" action="{{url('admin/employess/import')}}" enctype="multipart/form-data">
                {{csrf_field()}}
                  <div class="modal-body">
                    <div class="form-group">
                      <label for="exampleInputFile">Input File(DataSheet)</label>
                      <input type="file" name="file" class="form-control" id="exampleInputImage1"
                      accept=".csv, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel">
                    </div>
                  </div>
                  <!-- /.card-body -->

                  <div class="modal-footer justify-content-between">
                    <button data-dismiss="modal" class="btn btn-default">Cancel</button>
                    <button type="submit" class="btn btn-success">@lang('text.Submit')</button>
                  </div>
                </form>
          </div>
        </div>
    </div>

@stop
