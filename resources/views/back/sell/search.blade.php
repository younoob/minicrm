@extends('back.index')

@section('content')

<section class="content">
    <div class="row">
        <div class="col-12">

            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Data Sell</h3>

                    <form action="{{ url('admin/sell/search') }}" method="post" role="form" class="form-inline ml-3 float-right">
                        {{ csrf_field() }}
                        <div class="input-group input-group-sm">
                            <input type="text" class="form-control" name="qsell" aria-label="Search Sell" placeholder="Search Sell">
                            <div class="input-group-append">
                                <button class="btn btn-navbar" type="submit">
                                    <i class="fas fa-search"></i>
                                </button>
                            </div>
                        </div>
                    </form>
                </div>

                <div class="card-body">
                    <table class="table table-bordered table-striped">
                        <thead class="text-center">
                            <th>@lang('text.No')</th>
                            <th>Price</th>
                            <th>Item</th>
                            <th>Discount</th>
                            <th>Employee</th>
                            <th>Created</th>
                            <th>@lang('text.Action')</th>
                        </thead>
                        <tbody>
                            @if($keyword)
                                @forelse($sell as $index => $data)
                                    <tr>
                                        <td>{{++$index}}</td>
                                        <td>{{ $data->getItemName ? $data->getItemName->name: 'unknown' }}</td>
                                        <td>@currency($data->price)</td>
                                        <td>{{ $data->discount }}</td>
                                        <td>
                                            {{ $data->getEmployeeName ? $data->getEmployeeName->fisrt_name: 'unknown' }}
                                            {{ $data->getEmployeeName ? $data->getEmployeeName->last_name: 'unknown' }}
                                        </td>
                                        <td>{{ \Carbon\Carbon::parse($data->created_at)->setTimezone(Session::get('timezone')) }}</td>
                                        <td></td>
                                    </tr>
                                @empty
                                <tr>
                                    <td class="text-center" colspan="6">No data found</td>
                                </tr>
                                @endforelse
                            @else
                            <tr>
                                <td class="text-center" colspan="6">Please input a keyword</td>
                            </tr>
                            @endif
                        </tbody>
                    </table>
                </div>

                <div class="d-flex justify-content-center">
                    {{ $sell->links() }}
                </div>
            </div>

        </div>
    </div>
</section>

@stop