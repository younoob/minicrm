@extends('back.index')

@section('content')

<div class="card card-default">
  <div class="card-header">
    <h3 class="card-title">Update Data Sell</h3>
  </div>

  @if(count($errors))
      @foreach($errors->all() as $errors)
          <div class="alert alert-danger">
            {{$errors}} <i class="fa fa-times float-right" data-dismiss="alert"></i>
          </div>
      @endforeach
  @endif

  <form method="post" action="{{ url('admin/sell/update/'.$sell->id) }}">
    {{csrf_field()}}
    <div class="card-body">
      <div class="row">
        <div class="col-md-6">
          <div class="form-group">
            <label>Item</label>
            <select name="item_id" class="form-control select2" style="width: 100%;">
              <option>Item</option>
              @forelse($item as $data)
              <option value="{{ $data->id }}" {{$data->id == $sell->item_id ? 'selected' : ''}}>{{ $data->name }}  ({{ $data->price }})</option>
              @empty
              <option>No data found</option>
              @endforelse
            </select>
          </div>
          <div class="form-group">
            <label>Employee</label>
            <select name="employee_id" class="form-control select2" style="width: 100%;">
              <option>Employee</option>
              <option selected="selected" value="{{$sell->employee_id}}">{{$sell->getEmployeeName ? $sell->getEmployeeName->fisrt_name: 'unknown'}} {{$sell->getEmployeeName ? $sell->getEmployeeName->last_name: 'unknown'}}</option>
              @forelse($employee as $data)
              <option value="{{ $data->id }}" {{ $data->id == $sell->employee_id ? 'selected' : '' }}>{{ $data->fisrt_name }} {{ $data->last_name }}</option>
              @empty
              <option>No data found</option>
              @endforelse
            </select>
          </div>
        </div>
        <div class="col-md-6">
          <div class="form-group">
            <label>Discount</label>
            <input type="number" name="discount" class="form-control" placeholder="Discount" step="0.01" value="{{ $sell->discount }}">
          </div>
        </div>
      </div>
    </div>
    <div class="card-footer">
          <button type="submit" class="btn btn-info">@lang('text.Update')</button>
          <a href="{{ url('/admin/sell') }}" class="btn btn-warning">@lang('text.Back')</a>
    </div>
  </form>
</div>

@stop