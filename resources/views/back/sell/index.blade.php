@extends('back.index')

@section('content')

<section class="content">
    <div class="row">
        <div class="col-12">

            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Data Sell</h3>

                    <form class="form-inline ml-3 float-right" role="form" method="post" action="{{ url('admin/sell/search') }}">
                      {{ csrf_field() }}
                      <div class="input-group input-group-sm">
                        <input class="form-control" type="search" placeholder="Search Sell" aria-label="Search" name="qsell">
                        <div class="input-group-append">
                          <button class="btn btn-navbar" type="submit">
                            <i class="fas fa-search"></i>
                          </button>
                        </div>
                      </div>
                    </form>
                </div>

                <div class="card-body">

                @if(session()->has('success'))
                <div class="alert alert-success">
                    {{ session('success') }} <i class="fa fa-times float-right" data-dismiss="alert"></i>
                </div>
                @endif

                @if(session()->has('error'))
                <div class="alert alert-danger">
                    {{ session('error') }} <i class="fa fa-times float-right" data-dismiss="alert"></i>
                </div>
                @endif

                    <table class="table table-bordered table-striped">
                        <thead class="text-center">
                            <tr>
                                <th>No</th>
                                <th>Item</th>
                                <th>Price</th>
                                <th>Discount</th>
                                <th>Employee</th>
                                <th>Created</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @forelse($sell as $index => $data)
                            <tr>
                                <td>{{ ++$index }}</td>
                                <td>{{ $data->getItemName ? $data->getItemName->name: 'unknown' }}</td>
                                <td>@currency($data->price)</td>
                                <td>{{ $data->discount }} <span>%</span></td>
                                <td>{{ $data->getEmployeeName ? $data->getEmployeeName->fisrt_name: 'unknown' }} {{ $data->getEmployeeName ? $data->getEmployeeName->last_name: 'unknown' }}</td>
                                <td>{{ \Carbon\Carbon::parse($data->created_at)->setTimezone(Session::get('timezone')) }}</td>
                                <td>
                                    <a href="{{ url('admin/sell/update/'.$data->id) }}" class="text-warning action-element" title="edit"><i class="fas fa-edit"></i></a>
                                    <a href="{{ url('admin/sell/delete/'.$data->id) }}" class="text-danger action-element" title="delete"><i class="fas fa-trash"></i></a>
                                </td>
                            </tr>
                            @empty
                            <tr>
                                <td class="text-center" colspan="7">No data found</td>
                            </tr>
                            @endforelse
                        </tbody>
                    </table>
                </div>

                <div class="d-flex justify-content-center">
                    {{ $sell->links() }}
                </div>

                <div class="card-footer">
                    <a href="{{ url('/admin/sell/create') }}" class="btn btn-block btn-sm btn-info col-md-2">@lang('text.Create')</a>
                </div>

            </div>

        </div>
    </div>
</section>

@stop