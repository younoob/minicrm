@extends('back.index')

@section('content')

    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">
              Result of 

              @if($keyword)
                "{{$keyword}}"
              @else
                <span>"???"</span>
              @endif
            </h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{url('/admin/en')}}">@lang('text.Home')</a></li>
              <li class="breadcrumb-item active">Page Search</li>
            </ol>
          </div>
        </div>
      </div>
    </div>

    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div class="card card-primary">
              <div class="card-body">
                <div class="row">
              @if($keyword)

                @foreach($user as $users)
                <div class="col-lg-3 col-6">
                  <div class="small-box bg-search">
                    <div class="inner">
                      <h4 class="font-weight-normal mb-3">{{$users->name}}</h4>
                      <h2 class="mb-5">{{$users->role}}</h2>
                      <h6 class="card-text"><a href="mailto:{{$users->email}}" class="text-white">{{$users->email}}</a></h6>
                    </div>
                    <div class="icon">
                      <i class="ion ion-person"></i>
                    </div>
                  </div>
                </div>
                @endforeach

                @foreach($company as $company)
                <div class="col-lg-3 col-6">
                  <div class="small-box bg-search2">
                    <div class="inner">
                      <h4 class="font-weight-normal mb-3">{{$company->name}}</h4>
                      <h2 class="mb-5"><a href="{{$company->website}}" class="text-white">{{$company->website ? $company->website: 'nothing'}}</a></h2>
                      <h6 class="card-text"><a href="mailto:{{$company->email}}" class="text-white">{{$company->email? $company->email: 'notfound'}}</a></h6>
                    </div>
                    <div class="icon">
                      <i class="ion"><ion-icon name="business-outline"></ion-icon></i>
                    </div>
                  </div>
                </div>
                @endforeach      
                
                @foreach($employess as $employess)
                <div class="col-lg-3 col-6">
                  <div class="small-box bg-search3">
                    <div class="inner">
                      <h4 class="font-weight-normal mb-3">{{$employess->fisrt_name}} {{$employess->last_name}}</h4>
                      <h2 class="mb-5">{{ $employess->getCompanyName ? $employess->getCompanyName->name: 'unknown' }}</h2>
                      <h6 class="card-text"><a href="mailto:{{$employess->email}}" class="text-white">{{$employess->email}}</a></h6>
                      <h6 class="card-text"><a href="tel:{{$employess->phone}}" class="text-white">{{$employess->phone}}</a></h6>
                    </div>
                    <div class="icon">
                      <i class="ion"><ion-icon name="people-outline"></ion-icon></i>
                    </div>
                  </div>
                </div>
                @endforeach

                @foreach($item as $item)
                <div class="col-lg-3 col-6">
                  <div class="small-box bg-search4">
                    <div class="inner">
                      <h4 class="font-weight-normal mb-3">{{ $item->name }}</h4>
                      <h2 class="mb-5">{{ $item->price }}</h2>
                    </div>
                    <div class="icon">
                      <i class="fas fa-tags"></i>
                    </div>
                  </div>
                </div>
                @endforeach

                @foreach($sell as $sell)
                <div class="col-lg-3 col-6">
                  <div class="small-box bg-search5">
                    <div class="inner">
                      <h4 class="font-weight-normal mb-3">{{ $sell->getItemName ? $sell->getItemName->name: 'unknown' }}</h4>
                      <div class="row">
                        <h2 class="mb-5 col-sm-8">{{ $sell->price }}</h2>
                        <p class="col-sm-4 float-right mb-8">{{ $sell->discount }}% <i class="fas fa-arrow-down"></i></p>
                      </div>
                    </div>
                    <div class="icon">
                      <i class="fas fa-shopping-cart"></i>
                    </div>
                  </div>
                </div>
                @endforeach
              
              @else
                <div class="d-flex justify-content-center">
                  <h3>Please Input a Keyword</h3>
                </div>
              @endif

              </div>
            </div>
          </div>
        </div>
      </div>
    </section>

    

@stop