@extends('back.index')

@section('content')

    @if(session()->has('message'))
      <div class="alert alert-info " data-dismiss="alert">
        <span>{{session('message')}} <i class="fa fa-times float-right" data-dismiss="alert"></i></span>
      </div>
    @endif

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <!-- Small boxes (Stat box) -->
        <div class="row">
          <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box bg-info">
              <div class="inner">
                <h3>
                  @forelse($employess as $index => $employess )
                    @if($loop->last)
                      {{ $index+1 }}
                    @endif
                  @empty
                    0
                  @endforelse
                </h3>

                <p>@lang('text.Employess')</p>
              </div>
              <div class="icon">
                <i class="ion ion-bag"></i>
              </div>
              <a href="{{url('admin/employess')}}" class="small-box-footer">@lang('text.More Info')<i class="fas fa-arrow-circle-right"></i></a>
            </div>
          </div>
          <!-- ./col -->
          <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box bg-success">
              <div class="inner">
                <h3>
                  @forelse($company as $index => $company)
                    @if($loop->last)
                      {{ $index+1 }}
                    @endif
                  @empty
                    0
                  @endforelse
                </h3>

                <p>@lang('text.Company')</p>
              </div>
              <div class="icon">
                <i class="ion ion-stats-bars"></i>
              </div>
              <a href="{{url('admin/company')}}" class="small-box-footer">@lang('text.More Info') <i class="fas fa-arrow-circle-right"></i></a>
            </div>
          </div>
          <!-- ./col -->
          <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box bg-warning">
              <div class="inner">
                <h3>
                  @forelse($user as $index => $user)
                    @if($loop->last)
                      {{ $index+1 }}
                    @endif
                  @empty
                    0
                  @endforelse
                </h3>

                <p>@lang('text.User Registration')</p>
              </div>
              <div class="icon">
                <i class="ion ion-person-add"></i>
              </div>
              <a href="{{url('/admin/users')}}" class="small-box-footer">@lang('text.More Info')<i class="fas fa-arrow-circle-right"></i></a>
            </div>
          </div>
          <!-- ./col -->
          <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box bg-danger">
              <div class="inner">
                <h3>
                  @forelse($item as $index => $item)
                    @if($loop->last)
                      {{ $index+1 }}
                    @endif
                  @empty
                    0
                  @endforelse
                </h3>

                <p>Item</p>
              </div>
              <div class="icon">
                <i class="ion ion-pricetags"></i>
              </div>
              <a href="{{url('/admin/item')}}" class="small-box-footer">@lang('text.More Info')<i class="fas fa-arrow-circle-right"></i></a>
            </div>
          </div>
          <!-- ./col -->
          <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box bg-success">
              <div class="inner">
                <h3>
                  @forelse($sell as $index => $sell)
                    @if($loop->last)
                      {{ $index+1 }}
                    @endif
                  @empty
                    0
                  @endforelse
                </h3>

                <p>Sell</p>
              </div>
              <div class="icon">
                <i class="fas fa-shopping-cart"></i>
              </div>
              <a href="{{url('/admin/sell')}}" class="small-box-footer">@lang('text.More Info')<i class="fas fa-arrow-circle-right"></i></a>
            </div>
          </div>
           <!-- ./col -->
           <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box bg-success">
              <div class="inner">
                <h3>
                  @forelse($summary as $index => $summary)
                    @if($loop->last)
                      {{ $index+1 }}
                    @endif
                  @empty
                    0
                  @endforelse
                </h3>

                <p>Sell Summary</p>
              </div>
              <div class="icon">
                <i class="fas fa-comment-dollar"></i>
              </div>
              <a href="{{url('/admin/summary')}}" class="small-box-footer">@lang('text.More Info')<i class="fas fa-arrow-circle-right"></i></a>
            </div>
          </div>
          <!-- ./col -->
        </div>
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->

    <script>

      
    </script>

@stop
