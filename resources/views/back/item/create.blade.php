@extends('back.index')

@section('content')

<div class="card card-info">
    <div class="card-header">
    <h3 class="card-title">Create Data Item</h3>
    </div>

    <form method="post" action="{{ url('/admin/item/create') }}">
    {{csrf_field()}}
    <div class="card-body">

        <div class="input-group">
            <div class="input-group-prepend">
                <span class="input-group-text"><i class="fas fa-envelope"></i></span>
            </div>
            <input name="name" type="text" class="form-control" placeholder="Name">
        </div>
        @error('name')
            <span class="text-danger text-sm">{{$message}}</span>
        @enderror

        <div class="input-group mt-3">
            <div class="input-group-prepend">
                <span class="input-group-text"><i class="fas fa-dollar-sign"></i></span>
            </div>
            <input name="price" type="text" class="form-control" placeholder="Price">
        </div>
        @error('price')
            <span class="text-danger text-sm">{{$message}}</span>
        @enderror

    </div>

    <div class="card-footer">
        <button type="submit" class="btn btn-primary">@lang('text.Create')</button>
        <a href="{{ url('/admin/item') }}" class="btn btn-warning">@lang('text.Back')</a>
    </div>
    </form>
</div>

@stop
