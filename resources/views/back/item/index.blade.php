@extends('back.index')

@section('content')

<section class="content">
    <div class="row">
        <div class="col-12">

            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Data Items</h3>

                    <form class="form-inline ml-3 float-right" role="form" method="post" action="{{ url('admin/item/search') }}">
                      {{ csrf_field() }}
                      <div class="input-group input-group-sm">
                        <input class="form-control form-control-navbar" type="search" placeholder="Search Item" aria-label="Search" name="qitem">
                        <div class="input-group-append">
                          <button class="btn btn-navbar" type="submit">
                            <i class="fas fa-search"></i>
                          </button>
                        </div>
                      </div>
                    </form>
                </div>

                <div class="card-body">
                    <table class="table table-bordered table-striped">
                        <thead class="text-center">
                            <tr>
                                <th>No</th>
                                <th>Name</th>
                                <th>Price</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @forelse ($item as $index => $data)
                            <tr>
                                <td class="text-center">{{ ++$index }}</td>
                                <td>{{ $data->name }}</td>
                                <td>@currency($data->price)</td>
                                <td class="text-center">
                                    <a href="{{ url('/admin/item/update/'.$data->id) }}" class="text-warning action-element" title="edit"><i class="fa fa-edit"></i></a>
                                    <a href="{{ url('/admin/item/delete/'.$data->id) }}" class="text-danger action-element" title="delete"><i class="fa fa-trash"></i></a>
                                </td>
                            </tr>
                            @empty
                            <tr>
                                <td colspan="4" class="text-center">No data found</td>
                            </tr>
                            @endforelse
                        </tbody>
                    </table>
                </div>

                <div class="d-flex justify-content-center">
                    {{ $item->links() }}
                </div>

                <div class="card-footer">
                    <a href="{{ url('/admin/item/create') }}" class="btn btn-block btn-info btn-sm col-md-2">@lang('text.Create')</a>
                </div>
            </div>


        </div>
    </div>
</section>

@stop
