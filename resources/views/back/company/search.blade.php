@extends('back.index')

@section('content')

<section class="content">

      <div class="row">
        <div class="col-12">

          <div class="card">
            <div class="card-header">
              <h3 class="card-title">@lang('text.Data Company')</h3>

              <form class="form-inline ml-3 float-right" role="form" method="post" action="{{ url('admin/company/search') }}">
                {{ csrf_field() }}

                <div class="row">
                  <div class="col-sm-5">
                    <div class="input-group input-group-sm">
                      <div class="input-group-prepend">
                        <span class="input-group-text">
                          <i class="far fa-calendar-alt"></i>
                        </span>
                      </div>
                      <input type="text" class="form-control float-right" name="date_range" id="reservation">
                    </div>
                  </div>

                  <div class="col-sm-5">
                    <div class="input-group input-group-sm">
                      <input class="form-control" type="search" placeholder="Search Company" aria-label="Search Company" name="qcompany">
                    </div>
                  </div>

                  <div class="col-sm-2">
                    <button class="btn btn-navbar" type="submit">
                      <i class="fas fa-search"></i>
                    </button>
                  </div>

                </div>

              </form>

            </div>
            <!-- /.card-header -->
            <div class="card-body">

              <table class="table table-bordered table-striped">
                <thead class="text-center">
                <tr>
                  <th>@lang('text.No')</th>
                  <th>@lang('text.Name')</th>
                  <th>@lang('text.Email')</th>
                  <th>@lang('text.Logo')</th>
                  <th>@lang('text.Website')</th>
                  <th>@lang('text.Joined')</th>
                  <th>@lang('text.Action')</th>
                </tr>
                </thead>
                <tbody>
                @if($keyword || $date)
                    @forelse($companies as $index => $company)
                    <tr>
                      <td>{{++$index}}</td>
                      <td>{{$company->name}}</td>
                      <td><a href="mailto:{{$company->email}}">{{$company->email}}</a></td>
                      <td class="text-center">
                        <a href="{{$company->image}}" target="blank"><img width="50px" src="{{$company->image}}" alt="Company Logo"></a>
                      </td>
                      <td><a href="{{$company->website}}" target="blank">{{$company->website ? $company->website: 'nothing'}}</a></td>
                      <td>{{ \Carbon\Carbon::parse($company->created_at)->setTimezone(Session::get('timezone')) }}</td>
                      <td>
                        <a href="{{url('/admin/company/update/'.$company->id)}}" title="edit" class="text-warning action-element"><i class="fa fa-edit"></i></a>
                        <a href="{{url('/admin/company/delete/'.$company->id)}}" title="delete" class="text-danger action-element"><i class="fa fa-trash"></i></a>
                        <a href="{{url('/admin/company/details/'.$company->id)}}" title="details" class="text-info action-element"><i class="fas fa-info-circle"></i></a>
                      </td>
                    </tr>
                    @empty
                    <tr>
                        <td class="text-center" colspan="7">No data found</td>
                    </tr>
                    @endforelse
                @else
                <tr>
                    <td class="text-center" colspan="7">Please input a keyword</td>
                </tr>
                @endif
                </tbody>
              </table>
            </div>

            <div class="d-flex justify-content-center">
              {{ $companies->links() }}
            </div>

            <div class="card-footer row">
                <a href="{{url('/admin/company/')}}" class="btn btn-block btn-info btn-sm col-md-2">@lang('text.Back')</a>
            </div>
          </div>
        </div>
      </div>
    </section>

@stop
