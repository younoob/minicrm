@extends('back.index')

@section('content')

<!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <!-- left column -->
          <div class="col-md-12">
            <!-- general form elements -->
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">@lang('text.Create Data Company')</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form role="form" method="post" action="{{url('admin/company/create')}}" enctype="multipart/form-data">
              {{csrf_field()}}
                <div class="card-body">

                 <div class="form-group">
                    <label for="exampleInputName1">@lang('text.Name')</label>
                    <input type="text" name="name" class="form-control" id="exampleInputName1" placeholder="Company Name">
                    @error('name')
                        <span class="text-danger text-sm">{{$message}}</span>
                    @enderror
                  </div>
                  <div class="form-group">
                    <label for="exampleInputEmail1">@lang('text.Email')</label>
                    <input type="email" name="email" class="form-control" id="exampleInputEmail1" placeholder="Enter email">
                    @error('email')
                        <span class="text-danger text-sm">{{$message}}</span>
                    @enderror
                  </div>
                  <div class="form-group">
                    <label for="exampleInputFile">@lang('text.Logo')</label>
                    <input type="file" name="image" class="form-control" id="exampleInputImage1" accept="image/png, image/jpg, image/jpeg, image/svg">
                    @error('image')
                        <span class="text-danger text-sm">{{$message}}</span>
                    @enderror
                  </div>
                  <div class="form-group">
                    <label for="exampleInputWebsite1">@lang('text.Website')</label>
                    <input type="text" name="website" class="form-control" id="exampleInputWebsite1" placeholder="Company Website">
                    @error('website')
                        <span class="text-danger text-sm">{{$message}}</span>
                    @enderror
                  </div>
                  <div class="form-group" hidden>
                    <label for="UserID">UserID</label>
                    <input type="text" class="form-control" placeholder="User ID" name="created_by" value="{{Auth::user()->id}}">
                  </div>
                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">@lang('text.Submit')</button>
                  <a href="{{ url('admin/company') }}" class="btn btn-warning">@lang('text.Back')</a>
                </div>
              </form>
            </div>
            <!-- /.card -->

          </div>
          <!--/.col (left) -->
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->

@stop
