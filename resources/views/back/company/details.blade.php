@extends('back.index')

@section('content')

 <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Details</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{ url('admin') }}">@lang('text.Home')</a></li>
              <li class="breadcrumb-item active">@lang('text.Details')</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">@lang('text.Company Details')</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <div class="row">
                  <div class="col-sm-4">
                    <div class="position-relative py-3">
                        @if (!$company->image)
                            <img src="{{ asset('image/office.png')}}" class="img-fluid img-circle shadow" width="500px" alt="Default Image">
                        @else
                            <img src="{{$company->image}}" class="img-fluid img-circle shadow" width="500px" alt="Company Image">
                        @endif
                    </div>
                  </div>
                  <div class="col-sm-8 float-right">
                      <div class="text-center">
                        <h1><strong>{{$company->name}}</strong></h1>
                      </div>
                      <div class="py-6">
                          <div class="col-lg-12">
                            <table>
                              <tr>
                                <th>@lang('text.Website')</th>
                                <th width="20">:</th>
                                <td>
                                    @if (!$company->website)
                                        <span>-</span>
                                    @else
                                        <a href="{{$company->website}}">{{$company->website}}</a>
                                    @endif
                                </td>
                              </tr>
                              <tr>
                                <th>@lang('text.Email')</th>
                                <th width="20">:</th>
                                <td><a href="mailto:{{$company->email}}">{{$company->email}}</a></td>
                              </tr>
                              <tr>
                                <th>@lang('text.Joined')</th>
                                <th width="20">:</th>
                                <td>{{ \Carbon\Carbon::parse($company->created_at)->setTimezone(Session::get('timezone')) }}</td>
                              </tr>
                              <tr>
                                <th>@lang('text.Created By')</th>
                                <th width="20">:</th>
                                <td width="150">{{ \Carbon\Carbon::parse($company->created_at)->setTimezone(Session::get('timezone')) }}</td>
                                <th>({{$company->getCreatedName ? $company->getCreatedName->name : 'uknown'}})</th>
                              </tr>
                              <tr>
                                <th>@lang('text.Updated By')</th>
                                <th width="20">:</th>
                                <td width="150"> {{ \Carbon\Carbon::parse($company->updated_at)->setTimezone(Session::get('timezone')) }}</td>
                                <th> ({{$company->getUpdatedName ? $company->getUpdatedName->name : 'uknown'}})</th>
                              </tr>
                            </table>
                          </div>
                          <br>
                          <br>
                          <br>
                          <br>
                          <a href="{{ url('admin/company') }}" class="btn btn-warning">@lang('text.Back')</a>
                      </div>
                  </div>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->

@stop
