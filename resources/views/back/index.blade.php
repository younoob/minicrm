<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>MiniCRM | Dashboard</title>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="{{asset('style/plugins/fontawesome-free/css/all.min.css')}}">
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
   <link rel="stylesheet" href="{{asset('style/plugins/datatables-bs4/css/dataTables.bootstrap4.css')}}">
   <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/flag-icon-css/3.3.0/css/flag-icon.min.css">
  <link rel="stylesheet" href="{{asset('style/plugins/sweetalert2-theme-bootstrap-4/bootstrap-4.min.css')}}">
  <link rel="stylesheet" href="{{asset('style/plugins/toastr/toastr.min.css')}}">
  <link rel="stylesheet" href="{{asset('style/plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css')}}">
  <link rel="stylesheet" href="{{asset('style/plugins/icheck-bootstrap/icheck-bootstrap.min.css')}}">
  <link rel="stylesheet" href="{{asset('style/plugins/jqvmap/jqvmap.min.css')}}">
  <link rel="stylesheet" href="{{asset('style/dist/css/adminlte.min.css')}}">
  <link rel="stylesheet" href="{{asset('style/plugins/overlayScrollbars/css/OverlayScrollbars.min.css')}}">
  <link rel="stylesheet" href="{{asset('style/plugins/summernote/summernote-bs4.css')}}">
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
  <link rel="stylesheet" href="{{asset('style/plugins/daterangepicker/daterangepicker.css')}}">
</head>
<style>
  .fa{
    cursor: pointer;
  }

  .modal-info{
    border: none;
    background: none;
  }

  .action-element{
    padding: 2px;
  }

  .bg-search{
    background-color: #FBDA61;
    background-image: linear-gradient(45deg, #FBDA61 0%, #FF5ACD 100%);
  }

  .bg-search2{
    background-color: #8EC5FC;
    background-image: linear-gradient(62deg, #8EC5FC 0%, #E0C3FC 100%);
  }

  .bg-search3{
    background-color: #8EC5FC;
    background-image: linear-gradient(62deg, #8EC5FC 0%, #E0C3FC 100%);
  }
  }

  .bg-search2{
    background-color: #8EC5FC;
    background-image: linear-gradient(62deg, #8EC5FC 0%, #E0C3FC 100%);
  }

  .bg-search3{
    background-color: #8EC5FC;
    background-image: linear-gradient(62deg, #8EC5FC 0%, #E0C3FC 100%);
  }

  .bg-search4{
    background: #ff9966;
    background: -webkit-linear-gradient(to right, #ff5e62, #ff9966);
    background: linear-gradient(to right, #ff5e62, #ff9966);

  }

  .bg-search5{
    background: #74ebd5;
    background: -webkit-linear-gradient(to right, #ACB6E5, #74ebd5);
    background: linear-gradient(to right, #ACB6E5, #74ebd5);
  }
</style>
<body class="hold-transition sidebar-mini layout-fixed">
<div class="wrapper">

  <!-- Navbar -->
  @include('back.components.header')

  <!-- Main Sidebar Container -->
  @include('back.components.sidebar')

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Dashboard</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Dashboard</li>
            </ol>
          </div>
        </div>
      </div>
    </div>

    <section class="content">
      <div class="container-fluid">
        @yield('content')
      </div>
    </section>
  </div>
  <!-- /.content-wrapper -->
  @include('back.components.footer')

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
</div>

<script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
@include('sweetalert::sweetalert')
<script src="{{asset('style/plugins/jquery/jquery.min.js')}}"></script>
<script src="{{asset('style/plugins/jquery-ui/jquery-ui.min.js')}}"></script>
<script>
  $.widget.bridge('uibutton', $.ui.button)
</script>
<script src="{{asset('style/plugins/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
<script src="{{asset('style/plugins/chart.js/Chart.min.js')}}"></script>
<script src="{{asset('style/plugins/sparklines/sparkline.js')}}"></script>
<script src="{{asset('style/plugins/jqvmap/jquery.vmap.min.js')}}"></script>
<script src="{{asset('style/plugins/jqvmap/maps/jquery.vmap.usa.js')}}"></script>
<script src="{{asset('style/plugins/jquery-knob/jquery.knob.min.js')}}"></script>
<script src="{{asset('style/plugins/moment/moment.min.js')}}"></script>
<script src="{{asset('style/plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js')}}"></script>
<script src="{{asset('style/plugins/summernote/summernote-bs4.min.js')}}"></script>
<script src="{{asset('style/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js')}}"></script>
<script src="{{asset('style/dist/js/adminlte.js')}}"></script>
<script src="{{asset('style/dist/js/demo.js')}}"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="{{asset('style/plugins/datatables/jquery.dataTables.js')}}"></script>
<script src="{{asset('style/plugins/datatables-bs4/js/dataTables.bootstrap4.js')}}"></script>
<script src="{{asset('style/plugins/sweetalert2/sweetalert2.min.js')}}"></script>
<script src="{{asset('style/plugins/toastr/toastr.min.js')}}"></script>
<script type="module" src="https://unpkg.com/ionicons@5.5.2/dist/ionicons/ionicons.esm.js"></script>
<script nomodule src="https://unpkg.com/ionicons@5.5.2/dist/ionicons/ionicons.js"></script>
<script src="{{asset('style/plugins/daterangepicker/daterangepicker.js')}}"></script>


@yield('js')

<script>
  $(function () {
    $("#example1").DataTable({
      "paging": true,
      "lengthChange": true,
      "searching": true,
      "ordering": true,
      "info": true,
      "autoWidth": true,
    });
  });

  $(document).ready(function() {
    var url_param = {!! json_encode(url()->current()) !!} + '?change_timezone='
    var timezone_session = {!! json_encode(Session::get('timezone')) !!}

    $.ajax({
      url: 'http://worldtimeapi.org/api/timezone',
      dataType: "json",
      success: function(data) {
        var time_data = jQuery.parseJSON(JSON.stringify(data));
        $.each(time_data, function(k, v) {
          $('#timezone').append($('<option>', {
            value: url_param + v
          }).text(v))
        })
      }
    });

    $('#timezone').val(url_param+timezone_session).change()

    $('#timezone').bind('change', function() {
      var url = $(this).val();
        if (url != '') {
          window.location = url;
          console.log(url);
        }
      return false;
    });
  });

  var msg = '{{Session::get('alert')}}';
  var exist = '{{Session::has('alert')}}';
  if(exist){
    alert(msg);
  }
</script>

<script>
  $(function () {
    //Initialize Select2 Elements

    //Date range picker
    $('#reservation').daterangepicker()
    //Date range picker with time picker
    $('#reservationtime').daterangepicker({
      timePicker: true,
      timePickerIncrement: 30,
      locale: {
        format: 'MM/DD/YYYY hh:mm A'
      }
    })
    //Date range as a button
    $('#daterange-btn').daterangepicker(
      {
        ranges   : {
          'Today'       : [moment(), moment()],
          'Yesterday'   : [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
          'Last 7 Days' : [moment().subtract(6, 'days'), moment()],
          'Last 30 Days': [moment().subtract(29, 'days'), moment()],
          'This Month'  : [moment().startOf('month'), moment().endOf('month')],
          'Last Month'  : [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        },
        startDate: moment().subtract(29, 'days'),
        endDate  : moment()
      },
      function (start, end) {
        $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'))
      }
    )

  })
</script>

</body>
</html>
