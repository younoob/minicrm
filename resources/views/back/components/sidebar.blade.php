<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <button type="button" class="brand-link btn btn-info toastrDefaultInfo" id="toastrDefaultInfo" style="background: none; border: none;">
      <img src="{{asset('style/dist/img/app.jpeg')}}" alt="CRM" class="brand-image img-circle elevation-3"
           style="opacity: .8">
      <span class="brand-text font-weight-light">Mini CRM</span>
    </button>

    <!-- Sidebar -->
    <div class="sidebar">

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
          <li class="nav-item">
            <a href="{{url('/admin/')}}" class="nav-link">
              <i class="nav-icon fas fa-th"></i>
              <p>
                @lang('text.Dashboard')
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="{{url('/admin/role')}}" class="nav-link">
              <i class="nav-icon fas fa-user-check"></i>
              <p>
                Role Management
              </p>
            </a>
          </li>
          <li class="nav-header">Workspace</li>
          <li class="nav-item">
            <a href="{{url('admin/company')}}" class="nav-link">
              <i class="nav-icon far fa-calendar-alt"></i>
              <p>
                @lang('text.Company')
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="{{url('admin/employess')}}" class="nav-link">
              <i class="nav-icon far fa-user"></i>
              <p>
                @lang('text.Employess')
              </p>
            </a>
          </li>

          <li class="nav-header">Product</li>
          <li class="nav-item">
            <a href="{{url('admin/item')}}" class="nav-link">
               <ion-icon name="pricetags-outline" class="nav-icon"></ion-icon>
              <p>
                Item
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="{{url('admin/sell')}}" class="nav-link">
               <ion-icon name="cart-outline" class="nav-icon"></ion-icon>
              <p>
                Sell
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="{{url('admin/summary')}}" class="nav-link">
               <i class="nav-icon fa fa-comment-dollar"></i>
              <p>
                Sell Summary
              </p>
            </a>
          </li>
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>

  @section('js')
  <script type="text/javascript">

    const Toast = Swal.mixin({
      toast: true,
      position: 'top-end',
      showConfirmButton: true,
      timer: 3000
    });

  $('#toastrDefaultInfo').click(function() {
    toastr.info('Laravel Developer Test at PT.Agung Trisula Mandiri.')

    });

</script>
@endsection
