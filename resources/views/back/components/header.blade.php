<nav class="main-header navbar navbar-expand navbar-white navbar-light">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#"><i class="fas fa-bars"></i></a>
      </li>
    </ul>

    <form class="form-inline ml-3" role="form" method="post" action="{{ url('admin/search') }}">
      {{ csrf_field() }}
      <div class="input-group input-group-sm">
        <input class="form-control form-control-navbar" type="search" placeholder="Search Everywhere" aria-label="Search" name="q">
        <div class="input-group-append">
          <button class="btn btn-navbar" type="submit">
            <i class="fas fa-search"></i>
          </button>
        </div>
      </div>
    </form>

    <!-- Right navbar links -->
    <ul class="navbar-nav ml-auto">

       <!-- Timezone Dropdown Menu -->
       <li class="nav-item">
              <select id="timezone" class="form-control form-control-navbar">
                <option value="#" selected>Select timezone</option>
              </select>
        </li>

       <!-- Language Dropdown Menu -->
       <li class="nav-item">
          @if (count(config('app.available_locales')) > 1)
          <li class="nav-item dropdown d-md-down-none">

              <a class="nav-link" data-toggle="dropdown" href="#" role="button" aria-haspopup="true"aria-expanded="false">
                  <i class="flag-icon flag-icon-gb"></i>
              </a>
              <div class="dropdown-menu dropdown-menu-right">
                  @foreach (config('app.available_locales') as $langLocale => $langName)
                      <a class="dropdown-item"
                          href="{{ url()->current() }}?change_language={{ $langLocale }}">{{ strtoupper($langLocale) }}
                          ({{ $langName }})</a>
                  @endforeach
              </div>
          </li>
          @endif
        </li>

        <!-- Setting Dropdown Menu -->
      <li class="nav-item dropdown">
        <a class="nav-link" data-toggle="dropdown" href="#">
          <i class="fas fa-cog"></i>
        </a>
        <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
          <span class="dropdown-item dropdown-header">Setting</span>
          <div class="dropdown-divider"></div>
          <a href="{{ url('admin/setting/check-connection') }}" class="dropdown-item">
            <i class="fas fa-plug mr-2"></i> Check Connection
          </a>
          <a href="{{ url('admin/setting/ipLocation') }}" class="dropdown-item" title="go to debug assistant">
              <i class="fas fa-location-arrow"></i> User Locations
          </a>
        </div>
      </li>

      <li class="nav-item dropdown user-menu">
        <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown">
          <img src="{{asset('style/dist/img/profile.png')}}" class="user-image img-circle elevation-2" alt="User Image">
          <span class="d-none d-md-inline">
            @guest
            @else
            {{ Auth::user()->name }}
            @endguest
          </span>
        </a>
        <ul class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
          <!-- User image -->
          <li class="user-header bg-primary">
            <img src="{{asset('style/dist/img/profile.png')}}" class="img-circle elevation-2" alt="User Image">

            <p>
              @guest
              @else
              {{ Auth::user()->name }}
              <small>Member since {{ \Carbon\Carbon::parse(Auth::user()->created_at) }}</small>
              @endguest
            </p>
          </li>
          <!-- Menu Footer-->
          <li class="user-footer">
            <a href="{{ url('/logout') }}" class="btn btn-default btn-flat float-right">Sign out</a>
          </li>
        </ul>
      </li>

      <li class="nav-item">
        <a class="nav-link" data-widget="control-sidebar" data-slide="true" href="#">
          <i class="fas fa-th-large"></i>
        </a>
      </li>
    </ul>
  </nav>
