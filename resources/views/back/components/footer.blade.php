<footer class="main-footer">
    <strong>@lang('text.Copyright') &copy; 
        <script>
            document.write(new Date().getFullYear())
        </script>
        
    <a href="http://adminlte.io">AdminLTE.io</a>. @lang('text.Modified by') <a href="https://arifnofriadi.netlify.app">Arif Nofriadi</a></strong>
    @lang('text.All rights reserved').
    <div class="float-right d-none d-sm-inline-block">
      <b>@lang('text.Version')</b> 3.0.0
    </div>
  </footer>