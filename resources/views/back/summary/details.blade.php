@extends('back.index')

@section('content')

<section class="content">
    <div class="row">
        <div class="col-12">
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">Details Summary</h3>
            </div>
            <div class="card-body">
                
                <div class="row">
                    <div class="col-md-6">
                        <table>
                            <tr>
                                <th>Price Total</th>
                                <th class="col-2">:</th>
                                <td>@currency($summary->price_total)</td>
                            </tr>
                            <tr>
                                <th>Discount Total</th>
                                <th class="col-2">:</th>
                                <td>@currency($summary->discount_total)</td>
                            </tr>
                            <tr>
                                <th>Total</th>
                                <th class="col-2">:</th>
                                <td>@currency($summary->total)</td>
                            </tr>
                        </table>
                    </div>
                    <div class="col-md-6">
                        <table>
                            <tr>
                                <th>Name</th>
                                <th class="col-2">:</th>
                                <td>{{ $summary->getEmployeeName->fisrt_name }}
                                    {{ $summary->getEmployeeName->last_name }}
                                </td>
                            </tr>
                            <tr>
                                <th>Company</th>
                                <th class="col-2">:</th>
                                <td>{{$sell->first()->getEmployeeName->getCompanyName->name}}</td>
                            </tr>
                            <tr>
                                <th>Transaction Time</th>
                                <th class="col-2">:</th>
                                <td>
                                    {{ $summary->created_at }}
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>

                <br>

                <table class="table table-bordered table-striped">
                    <thead class="text-center">
                        <tr>
                            <th>Item</th>
                            <th>Price</th>
                            <th>Discount</th>
                        </tr>
                    </thead>
                    <tbody>
                        @forelse($sell as $data)
                        <tr>
                            <td>{{$data->getItemName->name}}</td>
                            <td>@currency($data->price)</td>
                            <td>{{ $data->discount }}%</td>
                        </tr>
                        @empty
                        <tr>
                            <td class="text-center" colspan="3">No data found</td>
                        </tr>
                        @endforelse
                    </tbody>
                </table>
            </div>
            <div class="card-footer">
                <a href="{{ url('admin/summary') }}" title="go back!" class="btn btn-warning">Back</a>
            </div>
        </div>

        </div>
    </div>
</section>

@stop