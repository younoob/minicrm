 @extends('back.index')

 @section('content')

<section class="content">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Data Summary</h3>

                    <form action="{{ url('admin/summary/search') }}" method="post" role="form" class="form-inline ml-3 float-right">
                        {{ csrf_field() }}
                        <div class="row">
                            <div class="col-sm-5">
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">
                                            <i class="far fa-calender-alt"></i>
                                        </span>
                                    </div>
                                    <input type="text" name="date_range" class="form-control" id="reservation">
                                </div>
                            </div>

                            <div class="col-sm-5">
                                <div class="input-group input-group-sm">
                                    <input type="text" name="qsummary" class="form-control" placeholder="Search Summary" aria-label="Search">
                                </div>
                            </div>

                            <div class="col-sm-2">
                                <button class="btn btn-navbar" type="submit">
                                    <i class="fas fa-search"></i>
                                </button>
                            </div>
                        </div>
                    </form>
                </div>

                <div class="card-body">
                    <table class="table table-bordered table-striped">
                        <thead class="text-center">
                            <tr>
                                <th>@lang('text.No')</th>
                                <th>Employee</th>
                                <th>Company</th>
                                <th>Price Total</th>
                                <th>Discount Total</th>
                                <th>Total</th>
                                <th>Date</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if($keyword || $date)
                                @forelse($summary as $index => $data)
                                <tr>
                                    <td class="text-center">{{ ++$index }}</td>
                                    <td>
                                        {{ $data->getEmployeeName ? $data->getEmployeeName->fisrt_name: 'unknown' }}
                                        {{ $data->getEmployeeName ? $data->getEmployeeName->last_name: 'unknown' }}
                                    </td>
                                    <td>{{ $data->getEmployeeName->getCompanyName->name }}</td>
                                    <td>@currency($data->price_total)</td>
                                    <td>@currency($data->discount_total)</td>
                                    <td>@currency($data->total)</td>
                                    <td>
                                        <a href="{{ url('admin/summary/detail/'.$data->id) }}" title="see details">
                                            {{ \Carbon\Carbon::parse($data->created_at)->setTimezone(Session::get('timezone')) }}
                                        </a>
                                    </td>
                                </tr>
                                @empty
                                <tr>
                                    <td class="text-center" colspan="5">No data found</td>
                                </tr>
                                @endforelse
                            @else
                            <tr>
                                <td class="text-center" colspan="5">Please input a keyword</td>
                            </tr>
                            @endif
                        </tbody>
                    </table>
                </div>
                
                <div class="d-flex justify-content-center">
                    {{ $summary->links() }}
                </div>

            </div>
        </div>
    </div>
</section>

 @stop