@extends('back.index')

@section('content')

<section class="content">
    <div class="row">
        <div class="col-12">

            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Data Sell Summary</h3>

                    <form action="{{ url('admin/summary/search') }}" method="post" class="form-inline ml-3 float-right" role="form">
                        {{ csrf_field() }}

                        <div class="row">
                            <div class="col-sm-6">
                                <div class="input-group input-group-sm">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">
                                          <i class="far fa-calendar-alt"></i>
                                        </span>
                                    </div>
                                    <input type="text" class="form-control float-right" name="date_range" id="reservation">
                                </div>
                            </div>

                            <div class="col-sm-6">
                                <div class="input-group input-group-sm">
                                 <input type="text" name="qsummary" class="form-control" aria-label="Search" placeholder="Search Summary">
                                    <div class="input-group-append">
                                        <button class="btn btn-navbar" type="submit">
                                            <i class="fas fa-search"></i>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>

                <div class="card-body">
                    <table class="table table-bordered table-striped">
                        <thead class="text-center">
                            <tr>
                                <th>No</th>
                                <th>Employee</th>
                                <th>Company</th>
                                <th>Price Total</th>
                                <th>Discount Total</th>
                                <th>Total</th>
                                <th>Date</th>
                            </tr>
                        </thead>
                        <tbody>
                            @forelse($summary as $index => $data)
                            <tr>
                                <td class="text-center">{{ ++$index }}</td>
                                <td>
                                    {{ $data->getEmployeeName ? $data->getEmployeeName->fisrt_name: 'unknown' }}
                                    {{ $data->getEmployeeName ? $data->getEmployeeName->last_name: 'unknown' }}
                                </td>
                                <td>{{ $data->getEmployeeName->getCompanyName->name }}</td>
                                <td>@currency($data->price_total)</td>
                                <td>@currency($data->discount_total)</td>
                                <td>@currency($data->total)</td>
                                <td class="text-sm"><a href="{{ url('admin/summary/detail/'.$data->id) }}" title="see detail">
                                       <span>Date:</span> {{ \Carbon\Carbon::parse($data->created_at)->setTimezone(Session::get('timezone'))->format('Y-M-d') }}
                                    </a>
                                    <span>/ Time:</span> {{ \Carbon\Carbon::parse($data->created_at)->setTimezone(Session::get('timezone'))->format('h:i:s') }}
                                </td>
                            </tr>
                            @empty
                            <tr>
                                <td colspan="7" class="text-center">No data found</td>
                            </tr>
                            @endforelse
                        </tbody>
                    </table>
                </div>

                <div class="d-flex justify-content-center">
                    {{ $summary->links() }}
                </div>

            </div>

        </div>
    </div>
</section>

@stop
