instruction:
1.git clone https://gitlab.com/younoob/minicrm.git
2.cd /minicrm
3.composer update
4.create database mini and config your database in .env file
5.run php artisan migrate --seed
6.run php artisan db:seed --class=ExcelSeeder
7.run php artisan serve
8.last view on your browser