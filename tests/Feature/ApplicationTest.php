<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use Illuminate\Http\UploadedFile;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use App\Models\User;
use App\Models\Company;
use App\Models\Employess;

class ApplicationTest extends TestCase
{
    use WithoutMiddleware;
    /**
     * A basic feature test example.
     *
     * @return void
     */

    public function dataCompany()
    {
        return [
            'name' => 'Company Test',
            'email' => 'company.test@gmail.com',
            'website' => 'https://company-test.com',
            'image' => UploadedFile::fake()->image('company.png', 100, 100)->size(100),
        ];
    }

    public function dataEmployee()
    {
        return [
            'fisrt_name' => 'Test',
            'last_name' => 'Employee',
            'email' => 'test@employee.com',
            'phone' => '112233445566',
            'company_id' => 1
        ];
    }

    public function test_load_company_page()
    {
        $this->withoutExceptionHandling();
        $this->withoutMiddleware();
        $response = $this->get('/admin/company');

        $response->assertStatus(200);
    }

    public function test_create_company()
    {
        $this->withoutExceptionHandling();
        $this->withoutMiddleware();
        $response = $this->post('admin/company/create', array_merge($this->dataCompany()));
        $response->assertStatus(302);
    }

    public function test_update_company()
    {
        $this->withoutExceptionHandling();
        $this->withoutMiddleware();
        $toCompany = Company::first();
        $response = $this->post('admin/company/update/' .$toCompany->id, array_merge($this->dataCompany(), ['email' => 'company@edit.com']));
        $response->assertStatus(302);
    }

    public function test_delete_company()
    {
        $this->withoutExceptionHandling();
        $this->withoutMiddleware();
        $toCompany = Company::first();
        $response = $this->get('admin/company/delete/' .$toCompany->id);
        $response->assertStatus(302);
    }

    public function load_employee_page()
    {
        $this->withoutExceptionHandling();
        $this->withoutMiddleware();
        $response = $this->get('admin/employess');
        $response->assertStatus(200);
    }

    public function test_create_employee()
    {
        $this->withoutExceptionHandling();
        $this->withoutMiddleware();
        $toCompany = Company::first();
        $response = $this->post('admin/employess/create', array_merge($this->dataEmployee(), ['company_id' => $toCompany->id]));
        $response->assertStatus(302);
    }

    public function test_delete_employee()
    {
        $this->withoutExceptionHandling();
        $this->withoutMiddleware();
        $toEmployee = Employess::first();
        $response = $this->get('admin/employess/delete/' .$toEmployee->id);
        $response->assertStatus(302);
    }
}
